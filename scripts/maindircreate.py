#!/usr/bin/python3

"""
Erzeuge symlinks zum aktuellen Ordner in allen Unterordnern.

Dabei sind es relative Links, das heißt, dass alle Links mehrere ../../ etc.
sind. Die Links heißen alle .maindir, sodass man Pfade von jedem Ort
immer gleich mit .maindir/Unterordner/Datei bezeichnen kann.

Fehler, wie bspw. fehlende Unterstützung von symlinks durch das Betriebssystem
oder das Dateisystem werden nicht abgefangen.
"""

import os
import os.path
import errno

# os.walk geht alle Ordner durch und folgt standardmäßig keinen symlinks
# (sehr gut!)
# os.walk verändert nicht den aktuellen Ordner
for (path, directories, files) in os.walk(os.curdir):
    try:
        # relpath gibt den Pfad vom aktuellen Ordner relativ zum Ordner
        # in den wir den Link tun möchten
        os.symlink(os.path.relpath(os.curdir, path),
                   # .maindir heißt der Link
                   os.path.join(path, ".maindir"))
    except OSError as e:
        if e.errno == errno.EEXIST:
            # der Link oder eine Datei mit dem Namen existiert bereits
            # zum Beispiel, weil dieses Skript schon einmal aufgerufen
            # wurde
            # dann entferne den alten Link
            os.remove(os.path.join(path, ".maindir"))
            os.symlink(os.path.relpath(os.curdir, path),
                       os.path.join(path, ".maindir"))
