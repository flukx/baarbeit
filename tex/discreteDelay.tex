%! TEX program = lualatex

\input{.maindir/tex/header/preamble-section}
% inputs the preamble only if necessary

\docStart
\subsection{Discrete Delay}\label{sec:discreteDelay}
%
Finally we have reached the point of considering the typical
delay differential equations and see how they fit into the
framework developed so far.
A \newTerm{delay differential equation with discrete delay} can
typically be written as
%
\begin{equation*}
  x'(t) = f(x(t + h_1), x(t + h_2), x(t + h_3), …, x(t + h_N))
\end{equation*}
%
with pairwise distinct delays $0 \geq h_1 > h_2 > … > h_N$ ($N ∈ ℕ$).
%h_i ∈ ℝ_{\leq 0}$ ($0 ≤ i ≤ N ∈ ℕ$).
Here it comes in handy to split the right hand side
into two factors. The first one is
%
\begin{align*}
  Θ &\colon \tstH ⟶ \tstD(ℝ; H^N) ⊆ \bigcap_{η ∈ ℝ_{>0}} \HiH[H^N]{η}0 \\
  Θ x &= (τ_{h_1} x, τ_{h_2} x, …, τ_{h_N} x)
\end{align*}
%
that encapsulates the pasts of the argument.
(Note that in Definition \ref{def:Time_translation} we only defined $τ_h$ for
$\HiH{\w}0$ spaces but it is obvious how to similarly define
time translation on arbitrary functions defined on the whole real line.)

The second factor is
%
\begin{align*}
  Φ \colon \tstD(ℝ; H^N) %\bigcap_{η ∈ ℝ_{>0}} \HiH[H^N]{η}0
  ⟶ \dual{\tstp+}.
\end{align*}
%
With this notation we can calculate the Lipschitz constant of $Θ$:
As shown in Lemma \ref{thm:propertiesTimeTranslation} the time translation
is continuous with
%
\begin{equation*}
  \opnorm{τ_h}{\HiH{\w}0} = \exp(h \w).
\end{equation*}
To use that for $Θ$ note that $\HiH[H^N]{\w}0 \isom (\HiH{\w}0)^N$.
Then
\begin{align*}
  \norm{Θ}_{L(\HiH{\w}0, \HiH[H^N]{\w}0)}^2 &≤
  Σ_{k = 1}^N \opnorm{τ_{h_k}}{\HiH{\w}0}^2 \\
  &= \exp(2\w \underbrace{h_1}_{≤ 0}) + Σ_{k = 2}^N \exp(2\w \underbrace{h_k}_{≤ h_2 < h_1 ≤ 0}) \\ % \lneq looks too similar to \leq
  & ≤ \exp(2 \w h_1) + (N - 1) \exp(2\w \underbrace{h_2}_{< 0}) \\
  & \begin{cases}
    \searrow 0 + 0 & h_1 < 0 \\
    \searrow 1 + 0 & h_1 = 0
  \end{cases}
  \quad (\w ⟶ ∞)
\end{align*}
%
% For $h_1 = 0$ this implies for $\w ⟶ ∞$: $\opnorm{Θ} \searrow 1$ and for
% $h_1 < 0$ we even have $\opnorm{Θ} \searrow 0$.
Since $\Lip{Φ∘Θ}
≤ \Lip{Φ}
\Lip{Θ}$
we also get two different conditions on $Φ$ depending on the
decision if the right hand side solely depends on the past or not:
%
\begin{theorem}[Discrete delay, {\autocite[Theorem~5.8][31]{TUD:HSDDE}}] \label{thm:Discrete_delay}
  Let $N ∈ ℕ$,
  let $0 ≥ h_1 > h_2 > … > h_N ∈ ℝ_{\leq 0}$ be the discrete delays,
  $s ∈ (0, 1)$ if $h_1 = 0$ or $s ∈ ℝ_{> 0}$ otherwise.
  Let $\w_0 ∈ ℝ_{> 0}$ and
  $Φ \colon \tstD(ℝ, H^N) ⟶ \dual{\tstp+}$ such that
  for all $\w ∈ ℝ_{> \w_0}$ there is $K ∈ ℝ_{> 0}$
  such that for all $u, w ∈ \tstD(ℝ, H^N)$ and
  $ψ ∈ \tstp+$ we have
  %
  \begin{equation*}
    \abs{Φ(0)(ψ)} ≤ K\norm{ψ}_{-\w, 1} \text{ and }
    \abs{Φ(u)(ψ) - Φ(w)(ψ)} ≤ s \norm{ψ}_{-\w, 1} \norm{u - w}_{\w, 0}.
  \end{equation*}
  %
  Call $Φ_{\w}$ and $Θ_{\w}$ the appropriate continuous extensions.
  Then for large enough $\w$ the equation
  \begin{equation*}
    \der u = Φ_{\w}(Θ_{\w} u)
  \end{equation*}
  has a unique solution $u ∈ \Hi{\w}0$ and the solution operator
  is causal.
\end{theorem} % end theorem Discrete delay
\begin{proof}
  As considered above the theorem we can choose $\w$ large enough
  for
  %
  \begin{equation*}
    \Lip{Φ∘Θ}
    ≤ \Lip{Φ}
    \Lip{Θ} < s · \frac 1s = 1
    \text{ since } \frac 1s > 1 \text{ for } h_1 = 0 \text{ and } \frac 1s > 0 \text{ for } h_1 < 0
  \end{equation*}
  By Theorem \ref{thm:Picard-Lindelöf} of Picard-Lindelőf this implies
  the unique solution of the considered equation with causality of the
  solution operator guaranteed by Theorem \ref{thm:Causalityofsolving}.
\end{proof}
\docEnd

