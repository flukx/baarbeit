%! TEX program = lualatex

\input{.maindir/tex/header/preamble-section}
% inputs the preamble only if necessary

\docStart

\subsection{Solution independent of the weight} \label{sec:weightIndependence}
%
All theorems about existence, uniqueness and causality depended on some choice of
the search space characterized by the weight $\w$.
Since the original differential equation is independent of $\w$
it is important to ask if the solution depends on $\w$.
First we check that the contraction $\der^{-1}F_{\w}$ does
not depend on $\w$:
%
\begin{lemma}\label{thm:contractionwindependent}
  Let $G \colon \tstH ⟶ \dual{\tstp+}$ such that $G$ has continuous % and causal
  extensions to functions $G_k \colon \HiH{\w_k}0 ⟶ \HiH{\w_k}{-1}$
  for $\w_2 ≥ \w_1$. For $v_k ∈ \HiH{\w_k}0$ ($k ∈ \{1,2\}$) and $a ∈ ℝ$ we have
  \begin{align*}
    \tro[>a] v_1 &∈ \HiH{\w_2}0 \\
    \tro[< a] v_2 &∈ \HiH{\w_1}0.
  \end{align*}
  For $w ∈ \HiH{\w_1}0 ∩ \HiH{\w_2}0$
  \begin{equation*}
    G_1 w = G_2 w ∈ \dual{\tstp+}. %\HiH{\w_1}0 ∩ \HiH{\w_2}0.
  \end{equation*}
  % With the assumption and notation from Theorem \ref{thm:thm:Lipschitzcausal}
  % let $\w_1 ≥ \w_2 ≥ \w_0$ and $w ∈ \HiH{\w_1}0 ∩ \HiH{\w_2}0$. Then
  % \begin{equation*}
  %   \der[\w_1]^{-1} F_{\w_1} w =
  %   \der[\w_2]^{-1} F_{\w_2} w.
  % \end{equation*}
  % Furthermore for any $v ∈ \HiH{\w_1}0$ we have $\tro v ∈ \HiH{\w_1}0$.
\end{lemma} % end lemma name
%
\begin{proof}
  To show the first statements consider the definitions for any $a ∈ ℝ$:
  \begin{align}
    \norm{\tro[>a] v_1}_{\w_2, 0}^2 &=
    ∫_{ℝ} \norm{\tro[>a]v_1(t)}_H^2 \exp(-2\w_2 t) \D t \nonumber \\
    &=∫_a^{∞} \norm{v_1(t)}_H^2 \exp(-2\w_1 t) \exp(-2(\underbrace{\w_2 - \w_1}_{≥ 0})t) \D t \nonumber \\
    &≤ ∫_a^{∞} \norm{v_1(t)}_H^2 \exp(-2 \w_2 t) \D t \exp(-2(\w_2 - \w_1) a) \nonumber \\
    &= \norm{v_1}_{\w_1, 0}^2 \underbrace{\exp(-2a (\w_2 - \w_1))}_{\definedas C_{a, 2} < ∞}
    \label{eq:normcomp}
    \intertext{and in the same way}
    \norm{\tro[<a] v_2}_{\w_1, 0}^2
    &≤ \norm{v_2}_{\w_2, 0}^2 \underbrace{\exp(-2a (\w_1 - \w_2))}_{\definedas C_{a, 1} < ∞}.
  \end{align}
  %
  Those two estimates together show that the norms of $\HiH{\w_1}0$
  and $\HiH{\w_2}0$ are comparable if we restrict ourselves to functions
  with compact support.
  Hence it is useful to define $w_a \definedas \chFct_{[-a, a]}w$ ($a > 0$)
  for $w ∈ \HiH{\w_1}0 ∩ \HiH{\w_2}0$.

  Since $G_1$ and $G_2$ are defined as the closure of $G$
  we have to find sequences of test functions converging to
  $w$ in both norms.
  As a first step consider any sequence $(φ_{a, k})_{k ∈ ℕ}$ in $\tstH$
  that converges to $\chFct_{[-a, a]} w$ in $\HiH{\w_1}0$.
  Because of the comparability of the norms it
  also converges to $\chFct_{[-a, a]} w$ in $\HiH{\w_2}0$.
  (Choose $(φ_{a, k})_{k ∈ ℕ}$ such that $\supp φ_{a, k} ⊆ [-a - 1, a + 1]$.)

  Since
  $\exp(-2 \w_i \mo)\norm{w_a}_H^2 \nearrow \exp(-2 \w_i \mo)\norm{w}_H^2$
  for $i ∈ \{1,2\}$ and $a → ∞$
  the monotone convergence theorem implies convergence of $w_a ⟶ w$ for $a → ∞$ in
  both norms. Hence the diagonal sequence $(φ_{k, k})_{k ∈ ℕ} ⟶ w$ in
  both norms for $k → ∞$.

  The assumed boundedness for $G$ carry over to $G_1$ and $G_2$.
  We can use it to show that the equality $G_1 (φ_{k, k}) = G(φ_{k, k}) = G_2 (φ_{k, k})$
  implies $G_1 w = G_2 w$:
  In order to consider $G_2(w)$ and $G_1(w)$ as elements of a common space,
  consider elements of $\HiH{\w_{i}}{-1}$ ($i ∈ \{1,2\}$) as elements of
  $\dual{\tstp+}$ by restriction.
  (Note that the restriction is injective since $\tstp+$ is dense in
  $\HiH{\w_{i}}1$ ($i ∈ \{1,2\}$).) % and
  %values $G_1$ and $G_2$ are continuous.)
  Let $ψ ∈ \tstp+$. Then
  \begin{align*}
    \abs{(G_2(w) - G_1(w))(ψ)}
    &≤ \abs{(G_2(w) - G_2(φ_{k, k}))(ψ)}
    + \abs{(G_2(φ_{k, k}) - G_1(φ_{k, k}))(ψ)} \\
    &\phantom {≤} + \abs{(G_1(φ_{k, k}) - G_1(w))(ψ)} \\
    &\leq \underbrace{\norm{G_2(w) - G_2(φ_{k, k})}_{\w_2, -1}}_{→ 0,\; k→∞} \norm{ψ}_{-\w_2, 1}
    + 0 \\
    &\phantom{\leq} + \underbrace{\norm{G_1(φ_{k, k}) - G_1(w)}_{\w_1, -1}}_{→ 0,\; k → ∞} \norm{ψ}_{-\w_1, 1}
    → 0 \text{ as } k → ∞
  \end{align*}
  because $G_1$ and $G_2$ are continuous and $φ_{k, k} → w$ in both norms.
  %   &≤ \opnorm{G_2}{H_{\w_2, 0}}
  %   \underbrace{\norm{w - φ_{k, k}}_{\w_2, 0}}_{⟶ 0,\, k ⟶ ∞}
  %   \norm{ψ}_{-\w_2, 0} + 0\\
  %   &\phantom{≤} + \opnorm{G_1}{H_{\w_1, 0}}
  %   \underbrace{\norm{φ_{k, k} - w}_{\w_1, 0}}_{⟶ 0,\, k ⟶ ∞}
  %   \norm{ψ}_{-\w_1, 0}
  %   ⟶ 0 \text{ as $k ⟶ ∞$}.
  % \end{align*}
\end{proof}
%
From here it is a small step to show that the solution is independent of the weight.
%
\begin{theorem}[Solution independent of the weight, {\autocite[Theorem~4.6][19]{TUD:HSDDE}}] \label{thm:solutionweightindependent}
  Let $\w_0 ∈ ℝ_{> 0}$,
  $F \colon \tstH ⟶ \dual{\tstp+}$ be
  such that for each
  $\w > \w_0$
  there exist
  $K_{\w} ∈ ℝ_{> 0}$
  and
  $s_{\w} ∈ (0, 1)$
  such that for all
  $u, w ∈ \tstH$ and $ψ ∈ \tstp+$
  \begin{equation*}
    \abs{F(0)(ψ)} ≤ K_{\w}\norm{ψ}_{- \w, 1} \text{ and }
    \abs{F(u)(ψ) - F(w)(ψ)} ≤ s_{\w} \norm{ψ}_{-\w, 1} \norm{u - w}_{\w, 0}.
  \end{equation*}
  For $\w_2 ≥ \w_1 ≥ \w_0$ let $w_{\w_k} ∈ \HiH{\w_k}0$ ($k ∈ \{1, 2\}$)
  be the solution to
  \begin{equation*}
    \der[\w_k] u = F_{\w_k}(u) ∈ \HiH{\w_k}{-1} \quad (k ∈ \{1, 2\}).
  \end{equation*}
  Then the solutions are the same:
  \begin{equation*}
    w_1 = w_2 ∈ \HiH{\w_1}0 ∩ \HiH{\w_2}0
  \end{equation*}
\end{theorem}
%
\begin{proof}
  Let $G_k \definedas \der[\w_k]^{-1}F_{\w_k}$ ($k ∈ \{0,1,2\}$) for simplicity.

  The solution $w_1$ is unique in $\HiH{\w_1}0$ for a single $\w$ because $G_1$
  is a contraction.
  In analogy to the proof for the uniqueness of fixed points of the contraction $G_1$
  we would like to use the (wrong) inequality
  %
  \begin{equation*}
    \norm{w_1 - w_2}_{\w_1, 0}
    = \norm{G_1(w_1) - G_1(w_2)}_{\w_1, 0}
    ≤ s_{\w_1} \norm{w_1 - w_2}_{\w_1, 0} \text{ with } s_{\w_1} < 1.
  \end{equation*}
  %
  We cannot use it directly since $w_2$ is not necessarily in the domain of $G_1$ and
  if it was we do not know if $G_1 (w_2) = w_2$.
  In the previous Lemma \ref{thm:contractionwindependent} we have seen though
  that $\tro w_2 ∈ \HiH{\w_1}0$ for any $a ∈ ℝ$. So we can consider
  %
  \begin{equation*}
    G_1(\tro w_1) - G_1(\tro w_2) = G_1(\tro w_1) - G_2(\tro w_2) \quad \text{by Lemma \ref{thm:contractionwindependent}.}
  \end{equation*}
  In general $G_1(\tro w_1)$ could be anything but due to causality and
  Theorem \ref{thm:Lipschitzcausal} we know
  %
  \begin{equation*}
    \tro G_k (\tro w_k) = \tro G_k (w_k) \quad (k = 1,2).
  \end{equation*}
  %
  Hence
  %
  \begin{align*}
    \tro &\left(G_1(\tro w_1) - G_1(\tro w_2)\right) \\
    &= \tro \left(G_1 (\tro w_1) - G_2(\tro w_2)\right) \\
    &= \tro G_1(w_1) - \tro G_2(w_2) \\
    &= \tro (w_1 - w_2)
  \end{align*}
  since $w_1$ and $w_2$ are fixed points of $G_1$ and $G_2$ respectively.
  Now we can use the contraction argument from the beginning:
  %
  \begin{align*}
    \norm{\tro(w_1 - w_2)}_{\w_1, 0}
    &= \norm{\tro\left(G_1(\tro w_1) - G_1(\tro w_2)\right)}_{\w_1, 0} \\
    &≤ \norm{G_1(\tro w_1) - G_1(\tro w_2)}_{\w_1, 0} \\
    &≤ s_{\w_1} \norm{\tro (w_1 - w_2)} \quad \text{with } s_{\w_1} < 1.
  \end{align*}
  %
  Only for $\tro w_1 = \tro w_2$ this is no contradiction.
  So $w_1$ and $w_2$ agree on $(-∞, a)$ for $a ∈ ℝ$ but $a$ was
  arbitrary, so $w_1 = w_2$.
\end{proof}

\docEnd
