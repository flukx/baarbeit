%! TEX program = lualatex
\input{.maindir/tex/header/preamble-section}

\docStart

\subsection{Spectrum of self-adjoint operators}
\label{sec:spectrum_of_self_adjoint_operators}
%
In order to solve differential equations one essential tool
is integration, namely the inverse of differentiation. It is well-known
that an anti-derivative is not unique. Speaking in terms of operators,
differentiation does not have an inverse.
To circumvent this issue we will use a slightly modified
version of the differential operator,
which is invertible (see Corollary \ref{thm:diffContinuouslyInvertible}).
Which type of \enquote{a little} is possible can be asked in
a much more general case where we arrive at the notion of spectra.
It is the generalisation of eigenvalues in the infinite dimensional setting.
%
\begin{definition}[Spectrum]\label{def:Spectrum}
  Let $T \colon X ⊇ \dom(T) ⟶ X$ be a densely defined operator on a Banach space.
  Then 
  \begin{equation*}
    σ(T) = \setDef{λ ∈ ℂ}{T - λ \text{ is not continuously invertible}}
  \end{equation*}
  is called the \newTerm{spectrum of $T$}.
  Continuously invertible includes that the domain of the inverse is all of $X$.
\end{definition} % end definition of Spectrum
\begin{note}%[e]\label{note:e}
  In the finite dimensional case a linear operator is invertible if and only
  if it is injective.
  Hence in this case the spectrum is the set of eigenvalues.
\end{note} % end note finite dimensional spectrum
%
\begin{lemma}[Real spectrum of self-adjoint operators, {\autocite[VII.2.16][355\psq]{DWerner:FuAna}}] \label{thm:Real_spectrum_of_self-adjoint_operators}
  The spectrum of a self-adjoint %densely defined closed
  operator $T$
  on a Hilbert space $H$ is contained in the real axis.
  For any $λ ∈ ℂ \without ℝ$ we have the bound
  \begin{equation} \label{eq:bound_inverse_noneigenvalue}
    \norm{(T - λ)^{-1}} \leq \Im (λ)
  \end{equation}
  for the operator norm of the inverse of $T - λ$.
\end{lemma} % end lemma Real spectrum of self-adjoint operators
%
\begin{proof}
  Let $T \colon H ⊇ \dom(T) ⟶ H$ be a self-adjoint operator operator on a Hilbert space $H$. This implies $T$ is densely defined and closed.

  Let $λ ∈ ℂ \without ℝ$. It is to be shown that $T - λ$ is bounded below
  (implies existence and boundedness of the inverse) and has dense range
  (implies together with boundedness that $\dom((T - λ)^{-1}) = H$).

  % TODO: vielleicht alles auf graphen runterbrechen? wird es dann eher straight-forward? %
  Let $x ∈ \dom(T)$ with $\norm x = 1$.
  In order to find a lower bound for $\norm {(T-λ)x}$
  it is unpractical that $T$ and $λ$ appear on
  both sides in the inner product $\norm {(T-λ)x}^2 = ⟨(T-λ)x, (T-λ)x⟩$.
  That is why one starts with the Cauchy Schwarz inequality:
  \begin{equation*}
    \norm{(T-λ) x} = \norm{ (T-λ) x} \norm{x} ≥ \abs{⟨T-λ) x, x⟩}
    \stackrel{*}= \abs{⟨Tx, x⟩ - λ} \stackrel{**}{≥} \abs{\Im(λ)} > 0.
  \end{equation*}
  Here $*$ uses $1 = \norm x = \norm x^2 = ⟨x,x⟩$ and
  $**$ uses that $T$ is self-adjoint by noting
  \begin{equation*}
    \conj{⟨x, Tx⟩} = ⟨Tx, x⟩ = ⟨x, Tx⟩ ⇒ ⟨Tx, x⟩ ∈ ℝ.
  \end{equation*}
  This shows a lower bound for $\norm {(T-λ)x}$ independent of $x$ and hence
  $T - λ$ is injective with bounded inverse $(T-λ)^{-1} \colon H ⊇ (T-λ)(\dom(T)) ⟶ H$.
  \eqref{eq:bound_inverse_noneigenvalue} follows.

  To show that the domain of the inverse is $H$
  use the trick to consider the orthogonal complement:
  Let $z ∈ H$ with $⟨(T-λ)x, z⟩ = 0$ for all $x ∈ \dom(T)$. Then
  \begin{align*}
    0 &= ⟨(T-λ) x , z⟩ = ⟨Tx, z⟩ - ⟨x, \conj{λ} z⟩ && \text{ for all $x$ in $\dom(T)$} \\
    ⇒ ⟨x, \conj{λ} z⟩ &= ⟨Tx, z⟩ && \text{ for all $x$ in $\dom(T)$} \\
    ⇒ z ∈ \dom(T^*) &= \dom(T) \text{ and } (T^* - \conj{λ})z = (T-\conj{λ})z = 0
    % ⟨(T-λ)x, z⟩ &= ⟨x, (T-\conj{λ})z⟩ \text{ for all $x$ in $\dom(T)$, which is dense in $H$.} \\
    % ⇒ (T - \conj{λ})z &= 0 \text{ for all $z ∈ \dom(T)$}.
  \end{align*}
  Since $T - \conj{λ}$ is injective by the first part of the proof, we have $z = 0$. % if $z ∈ \dom(T)$.
  %But $\dom(T)$ is dense in $H$ and hence dense in $(T-λ)(\dom(T))^{\perp}$.
  So $(T-λ)(\dom(T))^{\perp} = \{0\}$
  and therefore $T-λ$ has dense range.

  % $T$ is closed, hence $T-λ$ is closed as well. This is nothing else than the graph
  % $\gr(T-λ)$ is closed in $H \times H$.
  % The graph
  % $\gr((T-λ)^{-1})$ % = (\gr(T-λ))^{-1}$}
  % is therefore closed as well (since only the first and second
  % entry got swapped).
  Since $(T - λ)^{-1}$ is continuous
  $\dom( (T-λ)^{-1} ) = (T - λ)^{-1}(H)$
  is closed and dense in $H$ and therefore equal to $H$.
\end{proof}
%
\docEnd
