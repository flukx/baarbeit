%! TEX program = lualatex

\input{.maindir/tex/header/preamble-section}
% inputs the preamble only if necessary

\docStart

\subsection{Hilbert space valued \texorpdfstring{$\LpD{}$}{L2}-space}
%
The intention of introducing tensor products was to generalise the solution
theory from scalar differential equations to systems of differential equations
and even further to any Hilbert space valued functions, not just ${ℂ}^n$.
Now we will show that tensor products are indeed the suitable tool to
consider those systems.

When the ranges $H$ are finite-dimensional it can be
easily seen,
that $\Lp{ℝ} \tens H \isom \LpH{H}$ where here the last
is the space of square-integrable
vector-valued functions: every $f ∈ \LpH{H}$ can be
written as a vector of $\Lp{ℝ}$ functions hence
$\LpH{H}$ is a direct sum of $\dim(H)$ copies of $\Lp{ℝ}$.
In the infinite-dimensional case
this idea still works out but needs more justification.
%
\begin{theorem} \label{thm:L2tensH=L2H}
  Let $H$ be a complex Hilbert space and $(Ω, \Sigma, μ)$ a measure space. Then
  $\LpH[Ω, \Sigma, μ]{ℂ} \tens H$ is isometrically isomorphic to $\LpH[Ω, \Sigma, μ]{H}$,
  the space of square-integrable $H$-valued functions via the identification
  \begin{equation}\label{equ:L2ident}
    f \tens ϕ ↦ (x ↦ f(x)ϕ)
  \end{equation}
\end{theorem}
%
\begin{proof}
  Let $L = \LpH[Ω, \Sigma, μ]{ℂ}$ and $K = \LpH[Ω, \Sigma, μ]{H}$.

  The idea is the same as for the finite dimensional case:
  given an $s ∈ K$ and a basis of $H$,
  one can determine for every $ϕ$ of the basis
  and every $x ∈ Ω$ the portion of $ϕ$ in $s(x) ∈ H$: $⟨ϕ, s(x)⟩_H$.
  For all $x ∈ Ω$ together this gives
  a function $f_{ϕ} ∈ L$.
  By the given identification $s$ is identified with a pure tensor.
  % i.\,e.\ an element of a copy of $L$.

  Let $U$ be a mapping to $K$ defined for all pairs $f ∈ L$, $ϕ ∈ H$
  by the identification \eqref{equ:L2ident}.
  $U$ is isometric: % and by bilinearity also well-defined:
  \begin{align*}
    \norm{f \tens ϕ}_{L \tens H}^2 &= ⟨f,f⟩_L ⟨ϕ, ϕ⟩_H
    = \norm{ϕ}_H^2 ∫_{Ω} \abs f^2 \D{μ} \\
    &= ∫_{Ω} \abs f^2 \norm{ϕ}_H^2 \D{μ}
    = ∫_{Ω} \norm {f(x)ϕ}_H^2 \DwithMeasure {μ}{x}
    = \norm{U(f \tens ϕ)}_K^2.
  \end{align*}
  We can extend $U$ linearly to $L \atens H$.
  We have to show that it is a well-defined mapping.
  Let $0 = Σ_{i = 1}^n f_i \tens ϕ_i ∈ L \atens H$. (By \eqref{equ:LinearcombAreSums}
  this is covers all linear combinations of pure tensors.)
  We have to show, that $Σ_{i = 1}^n f_iϕ_i = 0$.
  Consider
  \begin{align*}
    ⟨Σ_{i = 1}^n f_i ϕ_i , Σ_{i = 1}^n f_j ϕ_j⟩_K
    &= Σ_{i = 1}^n Σ_{j = 1}^n ∫_{Ω} ⟨f_i ϕ_i, f_j ϕ_j⟩_H \D{μ} \\
    &= Σ_{i = 1}^n Σ_{j = 1}^n ∫_{Ω} f_i \conj{f_j} ⟨ϕ_i, ϕ_j⟩_H
    = Σ_{i = 1}^n Σ_{j = 1}^n ⟨f_i, g⟩_L ⟨ϕ_i, ψ⟩_H \\
    &= ⟨Σ_{i = 1}^n f_i \tens ϕ_i, Σ_{j = 1}^n f_j \tens ϕ_j⟩_{L \tens H}
    = ⟨0, 0⟩_{L \tens H} = 0.
  \end{align*}
  Since $⟨·, ·⟩_{K}$ is positive definite, $Σ_{i = 1}^n f_i ϕ_i = 0$.
  By linearity of $U$, $U$ is well-defined.
  

  % This can be done by considering
  % any $s ∈ \tilde K$ for a dense subset $\tilde K$ in $K$
  % and showing $⟨Σ_{i = 1}^n f_iϕ_i, s⟩_K = 0$.
  % We cannot evaluate this inner product directly for an arbitrary $s ∈ K$ but
  % for $s = g(x) ψ$ for a $g ∈ L$, $ψ ∈ H$ we see that
  % %
  % \begin{align*}
  %   ⟨Σ_{i = 1}^n f_i ϕ_i , s⟩_K
  %   &= Σ_{i = 1}^n ⟨f_i ϕ_i , g ψ⟩_K
  %   = Σ_{i = 1}^n ∫_{Ω} ⟨f_i ϕ_i, g ψ⟩_H \D{μ} \\
  %   &= Σ_{i = 1}^n ∫_{Ω} f_i \conj{g} ⟨ϕ_i, ψ⟩_H
  %   = Σ_{i = 1}^n ⟨f_i, g⟩_L ⟨ϕ_i, ψ⟩_H \\
  %   &= Σ_{i = 1}^n ⟨f_i \tens ϕ_i, g \tens ψ⟩_{L \tens H}
  %   = ⟨Σ_{i = 1}^n f_i \tens ϕ_i, s⟩_{L \tens H} \\
  %   &= ⟨0, s⟩ = 0
  % \end{align*}
  % %
  % Hence $⟨Σ_{i = 1}^n f_i ϕ_i, s⟩ = 0$ for all $s ∈ U(L \atens H)$.
  % It is to be shown, that those are dense in $K$:
  %
  %
  Next, we show that that $U$ is an isometry on $L \atens H$.
  Consider $f = Σ_{i = 1}^n f_i \tens ϕ_i ∈ L \atens H$.
  Then
  \begin{align*}
    \norm{Σ_{i = 1}^n f_i \tens ϕ_i}_{L \tens H}^2
    &= Σ_{i = 1}^n Σ_{j = 1}^n ⟨f_i, f_j⟩_L ⟨ϕ_i, ϕ_j⟩_H \\
    &= Σ_{i = 1}^n Σ_{j = 1}^n ∫_{Ω} f_i \conj{f_j} \D{μ} ⟨ϕ_i, ϕ_j⟩_H \\
    &= Σ_{i = 1}^n Σ_{j = 1}^n ∫_{Ω} ⟨f_i(x) ϕ_i, f_j(x) ϕ_j⟩_H \DwithMeasure{μ}{x} \\
    &= ∫_{Ω} Σ_{i = 1}^n Σ_{j = 1}^n ⟨f_i(x) ϕ_i, f_j(x) ϕ_j⟩_H \DwithMeasure{μ}{x} \\
    &= ∫_{Ω} \norm{Σ_{i = 1}^n f_i(x) ϕ_i}_H^2 \DwithMeasure{μ}{x}
    = \norm{Σ_{i = 1}^n f_i ϕ_i}_K^2
    = \norm{U \left(Σ_{i = 1}^n f_i \tens ϕ_i\right)}_K^2.
  \end{align*}
  Since $U$ is an isometry on $L \atens H$ it is in particular
  continuous on a dense subset of $L \atens H$ and can by
  Lemma \ref{thm:contExtension} be extended to the completion
  $L \tens H$ as an isometry. It remains to show that
  $U$ is surjective. For this we need to show that the range
  of $U$ on $L \atens H$ is dense in $K$:
  % he following lemma:
  %
  \begin{lemma}\label{thm:rangeDenseInK}
    $U(L \atens H)$ is dense in $K$.
  \end{lemma} % end lemma
  \begin{proof}
    Let $S$ be a orthonormal basis of $H$, possibly non-countable.
    Let $t ∈ K$. Let $t_s ∈ L$ with $t_s (x) = ⟨t(x), s⟩_H$ be the portion
    of $t$ in the direction $s$.
    % Suppose there exists $n ∈ ℕ$, such that
    Let 
    \begin{equation*}
      S_n = \setDef{s ∈ S}{\norm{t_s}_L > \frac 1n}.
    \end{equation*} % is infinite.
    % Then by Parseval's identity in $H$ (\autocite[][]{wiki:parseval})
    % $\norm{t(x)}_H = Σ_{s ∈ S} \abs{⟨t(x), s(x)⟩}^2$.
    Let $S' ⊆ S_n$ be finite.
    Then by Bessel's inequality, see \autocite[V.4.3][233\psq]{DWerner:FuAna}
    % Suppose $S$ is non-countable. Then
    \begin{align*}
      \norm t_K^2 &= ∫_{Ω} \norm {t(x)}_H^2 \D{μ}
      ≥ ∫_{Ω} Σ_{s ∈ S'} \abs{⟨t(x), s⟩}^2 \D{μ} \\
      &= Σ_{s ∈ S'} ∫_{Ω} \abs{⟨t(x), s⟩}^2 \D{μ}
      = Σ_{s ∈ S'} \norm {t_s}^2_L
      > \frac 1{n^2} Σ_{s ∈ S'} 1
    \end{align*}
    %
    If any $S_n$ was infinite, $\norm t_K^2$ would be unbounded, a contradiction.
    Hence
    \begin{equation*}
      \tilde S = \Union_{n ∈ ℕ} S_n = \setDef{s ∈ S}{t_s \neq 0} = \{s_1, …\}
    \end{equation*}
    is countable. By Parseval's identity in $H$, see \autocite[Satz V.4.9][236]{DWerner:FuAna}
    \begin{align*}
      \lim_{k ⟶ ∞} \norm {\, t - Σ_{i = 1}^k t_{s_i} s_i\; }_K^2
      &= \lim_{k ⟶ ∞} ∫_{Ω} \norm{\, t - Σ_{i ∈ ℕ}^k t_{s_i} s_i}_H^2 \D{μ} \\
      &= ∫_{Ω} \lim_{k ⟶ ∞} \norm{\, t - Σ_{i ∈ ℕ}^k t_{s_i} s_i}_H^2 \D{μ}
      = 0
    \end{align*}
    where the exchange of limit and integral is justified by the
    monotone convergence theorem
    together with Parseval's identity.
    Hence $t$ can be approximated by elements of $U(L \tens H)$,
    i.\,e.\ $U(L \tens H)$ is dense in $K$.
  \end{proof}

\end{proof}
\docEnd
