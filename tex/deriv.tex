%! TEX program = lualatex
\input{.maindir/tex/header/preamble-section}

\docStart
\subsection{Closure of the time derivative} \label{sec:closure_of_the_time_derivative}
In this section the time derivative $\derc$ on $\tst$
and the closure $\deriv$ on a larger domain will be discussed.
One main class of functions that is discussed in every
section of analysis are differentiable functions.
Here differentiation is introduced as a linear operator,
firstly only on smooth functions with compact support $\tst$.
As we will see in the following,
this operator can be closed and
in this way define a notion of weak differentiability.
%
\begin{definition}[{\autocite[Definition 2.1][7]{TUD:HSDDE}}] \label{def:d}
  Let
  \begin{align*}
    \derc ⁚ \Lp{ℝ} \supset \tst &⟶ \Lp{ℝ} \\ ϕ &↦ ϕ' \\
    \text{and define } \deriv &\definedas -\derc^*.
  \end{align*}
  Then $\derc$ is skew-symmetric, $\deriv$ is well-defined and
  $\cl{\derc} \subseteq \deriv$ by Lemma \ref{thm:ClosureOfSymmetricInAdjoint}.
  We call $f ∈ \dom(\deriv)$ \newTerm{weakly differentiable}.
\end{definition}
\begin{proof}
  The skew-symmetry of $\derc$ is just integration by parts.
  Let $ψ ∈ \tst$, 
  \begin{align*}
    ψ ↦ ⟨\derc ψ, ψ⟩
    &= \int_{ℝ} ψ'(x) \conj{ψ (x)} \D x
    = [ψ(x) \conj{ψ(x)}]_{-∞}^{+∞} - ∫_{ℝ} ψ(x) \conj{ψ'(x)} \D x \\
    &= - ∫_{ℝ} ψ(x) \conj{ψ'(x)} \D x
    = -⟨ψ, ψ'⟩ \\
    &= ⟨ψ, -\dercψ⟩ \text{ is continuous on $\tst$}.
  \end{align*}
  $\tst$ is dense in $\Lp{ℝ}$ by Theorem \ref{thm:TestfctsDense}.
  From Lemma \ref{thm:ClosureOfSymmetricInAdjoint}
  we get that $\im \derc \subset (\im \derc)^*$ but this implies
  directly $\derc \subset (-\im)(\im^*) \derc^* = - \derc^* = \deriv$ and the desired statement. 
\end{proof}
One would naturally expect that the closure of a symmetric
operator is self-adjoint but this is indeed not the case in general.
One example is given by differentiation on absolutely
continuous functions, see
\autocite[Example (a) after Definition VII.2.6][347\psq]{DWerner:FuAna} for details.
In our case of differentiation on test functions as
a subspace of $\Lp{ℝ}$ it is the case though, but the
argument takes several steps. For $\deriv$ being skew-self-adjoint,
we have to show $\deriv = -\deriv^* = -(-\derc^*)^* \stackrel{\ref{thm:closability}}= \cl{\derc}$. \label{arg:derivative_skewselfadjoint_reformulation}
That is, for any $u ∈ \dom(\deriv)$ we have to find a sequence
$(u_n)_{n ∈ ℕ}$ in $\tst$ such that $u_n → u$ and $\derc u_n → \deriv u$ in $\Lp{ℝ}$.

In Section \ref{sec:TestfctsDenseInL2} we have seen how to approximate
functions in $\Lp{ℝ}$ with test functions: truncate and convolute.
Since in the first step of truncation we want to stay in $\dom(\deriv)$
we cannot truncate with $\chFct_{[-n, n]}$. Instead use
\begin{equation}
  \label{eq:smoothcrop}
  \begin{split}  
    (η_n)_{n ∈ ℕ} &\text{ in $\tst$ such that} \\
    \supp(η_n) &\subset [-n, n], \\
    η_n(x) &\leq 1 \text{ for all } x ∈ ℝ, \\
    η_n(x) &= 1 \text{ for all } x ∈ [-n + 1, n - 1], \\
    \rest{η_n}{[-n, -n + 1]} &= \rest{η_m}{[-m, -m + 1]}(· + n - m) \text{ for all } n, m > 1, \\ 
    \text{and } \rest{η_n}{[n, n - 1]} &= \rest{η_m}{[m, m - 1]}(· - n + m) \text{ for all } n, m > 1, \\
    \text{Then } \supp{(η_n')} &\subset [-n, -n + 1] ∪ [n-1, n].
  \end{split}
\end{equation}
This is the formalisation of a bump that becomes
more and more stretched towards positive and negative infinity
as $n$ grows.
In particular $η_n'$ consists of the same two bumps that
move out towards positive and negative infinity as $n$ grows and hence
the $η_n'$s are uniformly bounded.

Together with the convolution we define with
$δ_m$ being a Friedrichs mollifier as in Definition \ref{def:delta_fct}
\begin{equation} \label{eq:uApprox}
  u_{n, m} \definedas δ_m * (η_n u).
\end{equation}
In order to relate $\deriv u_{n, m}$ to $\deriv u$ we have to check
how $\deriv$ interact with convolution and multiplication.
For both we have statements for smooth functions (product rule
and Theorem \ref{thm:ConvolutionInTestfcts})
which we
want to generalise.
%
\begin{lemma}[Product rule] \label{thm:Product_rule}
  Let $φ ∈ \tst$ and $v ∈ \dom(\deriv)$. Then $φv ∈ \dom(\deriv)$ and
  \begin{equation*}
    \deriv(φv) = (\deriv φ)v + φ(\deriv v) = φ'v + φ(\deriv v).
  \end{equation*}
\end{lemma} % end lemma Product rule
\begin{proof}
  Since $\deriv$ is defined as an adjoint operator we show
  the equation by testing it with any $ψ ∈ \tst$:
  \begin{align*}
    ⟨\deriv φ v + φ \deriv v, ψ⟩_{\Lp{ℝ}}
    &= ∫_{ℝ} φ' v \conj{ψ} + φ \deriv v \conj{ψ}
    = ⟨v, \conj{φ'} ψ⟩_{\Lp{ℝ}} + ⟨\deriv v, \conj{φ}ψ⟩_{\Lp{ℝ}} \\
    &= ⟨v, \conj{φ'} ψ⟩_{\Lp{ℝ}} - ⟨v, \deriv(\conj{φ}ψ)⟩_{\Lp{ℝ}} \\
    &= ⟨v, \conj{φ'} ψ⟩_{\Lp{ℝ}} - ⟨v, \conj{φ'}ψ + \conj{φ}ψ'⟩_{\Lp{ℝ}} \\
    &= - ⟨v, \conj{φ}ψ'⟩_{\Lp{ℝ}}
    = -∫_{ℝ} v φ \conj{ψ'}
    = -⟨φv, ψ'⟩_{\Lp{ℝ}}.
  \end{align*}
  Since $v$, $\deriv v$ are in $\Lp{ℝ}$ and $φ$ and $\deriv φ$ are bounded,
  $\deriv φ v + φ \deriv v$ is in $\Lp{ℝ}$ and hence $ψ ↦ ⟨φv, ψ'⟩_{\Lp{ℝ}}$
  is continuous on $\tst$. By definition of $\deriv = -\derc^*$,
  $φv ∈ \dom(\deriv)$ and $\deriv(φv) = \deriv φv + φ\deriv v$.
\end{proof}
\begin{lemma}[Differentiation of convolution] \label{thm:DifferentiationOfConvolution}
  Let $φ ∈ \tst$ and $u ∈ \dom(\deriv)$. Then $φ*u ∈ \dom(\deriv)$ and
  \begin{equation*}
    \deriv(φ * u) = (\deriv φ) * u = φ * (\deriv u).
  \end{equation*}
\end{lemma} % end lemma Differentiation of convolution
\begin{proof}
  $(\deriv φ) * u$ is in $\Lp{ℝ}$ since $\deriv φ ∈ \tst$ and
  $u ∈ \Lp{ℝ}$ and by \ref{thm:ConvolutionInTestfcts}
  we have $(\deriv φ) * u = \deriv(φ * u)$. To show the last equality
  we again test with $ψ ∈ \tst$. The use of the theorem of Fubini is
  justified since the last term is finite and all integrands are
  non-negative. Note that we use that (classical) differentiation
  and argument shift commutate
  \begin{align*}
    -⟨φ * u, \deriv ψ⟩_{\Lp{ℝ}}
    &= - ∫_{ℝ} ∫_{ℝ} φ(t) u(x-t) \D t \conj{ψ'(x)} \D x \\
    &= -∫_{ℝ} φ(t) ∫_{ℝ} u(x-t) \conj{ψ'(x)} \D x \D t & \text{(Fubini)}\\
    &= -∫_{ℝ} φ(t) ∫_{ℝ} u(y) \conj{ψ'(y + t)} \D y \D t & (y = x - t)\\
    &= ∫_{ℝ} φ(t) ∫_{ℝ} \deriv u(y) \conj{ψ(y + t)} \D y \D t & \text{(Definition $\deriv$ for $u$ and $ψ(· + t)$)} \\
    &= ∫_{ℝ} φ(t) ∫_{ℝ} \deriv u(x - t) \conj{ψ(x)} \D y \D t & (x = y + t) \\
    &= ∫_{ℝ} ∫_{ℝ} φ(t) \deriv u(x-t) \D t \conj{ψ(x)} \D x & \text{(Fubini)} \\
    &= ∫_{ℝ} (φ * \deriv u) \conj{ψ} \D x \\
    &= ⟨φ * \deriv u, ψ⟩_{\Lp{ℝ}}
  \end{align*}
  So, $\deriv(φ * u) = φ * (\deriv u)$.
\end{proof}
\begin{theorem}[$\deriv = \cl{\derc}$, {\autocite[Theorem~2.2][7]{TUD:HSDDE}}] \label{thm:dSelfadjoint}
  $\deriv$ is skew-self-adjoint, that is $\deriv = \cl{\derc}$
  by the argument on page \pageref{arg:derivative_skewselfadjoint_reformulation}.
\end{theorem} % end theorem $\deriv = \cl{\derc}$
\begin{proof}
  Let $u ∈ \dom(\deriv)$ and $u_{m, n}$ as in \eqref{eq:uApprox}.
  Then $u_{m, n} ∈ \tst$ for all $m, n ∈ ℕ$ and
  \begin{align*}
    \deriv u_{m, n} = \deriv(δ_m * (η_n u))
    \stackrel{\ref{thm:DifferentiationOfConvolution}}= δ_m * \deriv(η_n u)
    \stackrel{\ref{thm:Product_rule}}= δ_m * \left((\deriv η_n) u + η_n (\deriv u)\right).
  \end{align*}
  Let $ε > 0$. Since $\supp{\deriv η_n} \subset [-n, -n + 1] ∪ [n - 1, n]$
  and $\deriv η_n$ are uniformly bounded,
  and hence $\deriv η_n u → 0$ for $n → ∞$, there is $N_1 ∈ ℕ$, such that
  for all $n > N_1$, $\norm{\deriv η_n u}_{\Lp{ℝ}} < \frac{ε}4$.

  By the dominated convergence theorem (dominated by $\deriv u$)
  there is $N_2 ∈ ℕ$ such that $\norm{η_n \deriv u - \deriv u}_{\Lp{ℝ}} < \frac {ε}4$.

  By Theorem \ref{thm:ApproximationViaConvolution} there exists for every
  $n ∈ ℕ$ an $m_n ∈ ℕ$ such that
  \begin{equation*}
    \norm{δ_{m_n} * (\deriv η_n u + η_n\deriv u) - (\deriv η_n u + η_n\deriv u)}_{\Lp{ℝ}} < \frac{ε}2.
  \end{equation*}

  With those estimates and the triangle inequality we get
  \begin{align*}
    \norm{δ_{m_n} * (\deriv η_n u + η_n\deriv u) - \deriv u}_{\Lp{ℝ}} < \frac{ε}2 + \frac{ε}4 + \frac{ε}4 = ε
  \end{align*}
  for all $n > \max\{N_1, N_2\}$. Hence $\deriv u_{m_n,n} → \deriv u$ and $u_{m_n, n} → u$
  for $n → ∞$
  in $\Lp{ℝ}$. That is by definition that $u ∈ \dom(\cl{\derc})$ and $\cl{\derc}u = \deriv u$.
\end{proof}

\docEnd
