%! TEX program = lualatex
\input{.maindir/tex/header/preamble-section}

\docStart
% Now feel free to type your text.

% !TEX root = ../main.tex
\section{Density of
  \texorpdfstring{$\tst$}
  {test functions} in
  \texorpdfstring{$\Lp{ℝ}$}
  {L2(R)}
}
\label{sec:TestfctsDenseInL2}
Elements of the common Banach and Hilbert spaces like $\Lp{ℝ}$
are functions with a number of properties that makes them very
unhandy to work with: they are only defined as equivalence classes,
they are not differentiable or even continuous and not closed under
multiplication. So often one can only write down explicit formulas
for some special functions like differentiable ones. But then
it is often possible to extend the results to all of $\Lp{ℝ}$ by
the tools that the functional analysis provides via taking limits.
But in order to extend any results we have to know that we can
approximate functions in $\Lp{ℝ}$ via nice functions. The \enquote{nice}
functions that are usually used are smooth functions with compact support.
\begin{definition}[Testfunctions] \label{def:Testfunctions}
  Define for any Hilbert space $H$
  \begin{equation*}
    \tstH \definedas \setDef{f \colon ℝ → H}
    {
      \begin{gathered}
        \supp(f) \text{ is compact and } \\
        f^{(n)} \text{ is continuously differentiable for every } n ∈ ℕ
      \end{gathered}
    }.
  \end{equation*}
  If the range is not given, we take $H = ℂ$: $\tst = \tstD(ℝ, ℂ)$.
\end{definition} % end definition of Testfunctions
To investigate an arbitrary $f ∈ \Lp{ℝ}$,
we approximate $f$ with smooth functions with bounded support.
To be able to do that we truncate $f$ and \enquote{make it smooth}
with an operation called convolution.
For this procedure we need the following family of functions:
%
\begin{definition}[Friedrichs mollifier]\label{def:delta_fct}
  A sequence $(δ_n)_{n∈ℕ}$ in $\tst$ of smooth functions is called a \newTerm{Friedrichs mollifier} or \newTerm{$δ$-sequence} if the following holds for all $n ∈ ℕ$:
  \begin{align}
    δ_n &≥ 0 \label{eq:delta_fct_pos}, \\
    \supp(δ_n) &⊆ \left[-\frac1n, \frac1n\right] \label{eq:delta_fct_supp}, \\
    ∫_{ℝ}δ_n &= 1 \label{eq:delta_fct_int1}.
  \end{align}
\end{definition}
%
So the $δ_n$ are bumps around $0$ that get steeper while $n$ grows.
One might think of them as approximations of a \enquote{function}
that is zero on $ℝ\without\{0\}$ and such big infinity at $\{0\}$
that the integral is $1$ even though there is of course no such function.

The existence of such a sequence,
even the existence of any elements of $\tst$ is not completely obvious.
That is why we give one example:
%
\begin{example}[Friedrichs mollifier]
  Let $\hat {δ}⁚ℝ⟶ℂ$,
  $x ↦
  \begin{cases}
    \exp(-\frac 1{1-x^2}) & -1 < x < 1 \\
    0                     & \text{otherwise}
  \end{cases}$.
  Apparently $\supp(\hat {δ}) ⊆ [-1, 1]$.
  To show that all derivatives exist,
  note that on $(-1, 1)$ $\hat {δ}$ is smooth as a composition of smooth functions.
  We have to check that $\lim_{x ⟶ ± 1} \hat{δ}^{(k)}(x) = 0$ for every $k ∈ ℕ_0$.
  Consider any derivative of $\exp(-\frac1{1-x^2})$.
  It is of the form
  \begin{equation*}
    \exp\left(-\frac1{1-x^2}\right) \frac{P(x)}{(1-x^2)^n} \text{ with a polynomial }P.
  \end{equation*}
  For $x ⟶ ± 1$ the exponential term dominates over any polynomial and
  hence
  \begin{equation*}
    \lim_{x ⟶ ± 1} δ^{(k)}(x) = 0.
  \end{equation*}
  $\hat {δ}$ does not satisfy the integration conditions.
  Therefore consider
  \begin{align*}
    δ &:=\left(∫_{ℝ}\hat {δ}(x) \D x\right)^{-1} \hat {δ}, \\
    δ_n &⁚ ℝ ⟶ ℂ, x↦ n δ(nx) \qquad (n ∈ ℕ).
  \end{align*}
\end{example}

\begin{definition}[Convolution]\label{def:convolution}
  Let $f, g ∈ \Lp{ℝ}$. Then $f * g ⁚ ℝ ⟶ ℂ$ is called the \newTerm{convolution}
  of $f$ and $g$ and is defined by
  %
  \begin{align*}
    f * g ⁚ x ↦ \int_{ℝ} f(x-y) · g(y) dy
    \stackrel{z =x-y}= \int_{ℝ} f(z) · g(x-z) \D z
  \end{align*}
\end{definition}
\begin{lemma}
  The convolution is well-defined and commutative.
\end{lemma}
\begin{proof}
  With the notation from the definition, we see,
  that for every $x ∈ ℝ$,
  $x ↦ f(x-y)$ is in $\Lp{ℝ}$ and
  therefore $⟨x ↦ f(x-y), g⟩ = (f * g)(x) ∈ ℝ$.
  With the formula in the definition,
  we see that $f * g = g * f$.
\end{proof}
%
As noted after the Definition \ref{def:delta_fct},
one might think of
\begin{equation*}
  \lim_{n → ∞} δ_n \text{ as } ∞\chFct_{\{0\}}
  \text{ with } ∫_{ℝ}\lim_{n → ∞} δ_n(x) \D x = 1.
\end{equation*}
With that in mind, it sounds reasonable to hope that this calculation holds in some way:
\begin{align*}
  f * \left(\lim_{n → ∞} δ_n\right)(x)
  &= ∫ f(z) \left(\lim_{n → ∞} δ_n\right) (x - z) \D z \\ &= ∫\left(
  \begin{cases}
    0 & z \neq x \\
    f(x) (\lim_{n → ∞} δ_n)(x) & z = x
\end{cases}\right)
\D z
= f(x).
\end{align*}
Of course such a function does not exist and therefore this not correct
but it nevertheless captures the idea to approximate $f$ by
convoluting it with a Friedrichs mollifier. %$δ$-functions.
(To make this precise one needs distribution theory which we will
not go into detail here.) This is helpful since $f * δ_n$ is smooth.
%
\begin{theorem}[Convolution smoothes]\label{thm:ConvolutionInTestfcts}
  Let $f ∈ \Lp{ℝ}$, $g ∈ \tst$.
  Then $f * g ∈ \tstsmD{}{∞}(ℝ)$ and $(f*g)^{(n)} = (f*g^{(n)})$ for every $n ∈ ℕ$.
\end{theorem}
%
\begin{proof}
  Let $f ∈ \Lp{ℝ}$, $g ∈ \tst$. Then
  $(f * g) (x ) = ∫_{ℝ} f(z) · g(x-z) \D z$. Let $x, h ∈ ℝ$, $\abs h < 1$. Then
  %
  \begin{align*}
    \frac {(f*g)(x + h) - (f * g)(x)} h
    &= \frac {∫_{ℝ}f(z) g(x + h - z) \D z - ∫_{ℝ}f(z) g(x - z) \D z} h \\
    &= ∫_{ℝ}f(z) \frac {g(x + h - z) - g(x - z)}h \D z.
  \end{align*}
  %
  The function $g$ is continuously differentiable,
  hence $\frac {g(x + h - z) - g(x - z)}h ⟶ g'(x - z)$
  for every $z ∈ ℝ$.
  To apply the Lebesgue convergence theorem,
  there needs to exist a majorizing function.
  By the mean value theorem there exists for every
  $h, z ∈ ℝ$ an $ζ_{h,z}$ between $x + h - z$ and $x - z$
  with $\frac {g(x + h - z) - g(x - z)}h = g'(ζ_{h,z})$.
  The function $g'$ is bounded since it is continuous on a compact domain, hence
  \begin{align*}
    \abs{f(z) \frac {g(x + h - z) - g(x - z)}h}
    &≤ \abs {\chFct_{\supp (g)+ [-1, 1]} (z) f(z)}\sup_{y ∈ ℝ}\abs{g'(y)} \\
    \text{and }
    \abs {\chFct_{\supp (g)+ [-1, 1]} f}\sup_{y ∈ ℝ}\abs{g'(y)} &∈ L^1(ℝ)
  \end{align*}
  since for a finite measure space $Ω$ (here $Ω = \supp (g)+ [-1, 1]$),
  it holds that $\LpD(Ω)$ is a subspace of $\LpD[1](Ω)$.
  By the Lebesgue convergence theorem,
  %
  \begin{align*}
    \frac {(f*g)(x + h) - (f * g)(x)} h
    ⟶ ∫_{ℝ}f(z) g'(x - z) \D z = (f*g')(x).
  \end{align*}
  By induction, the statement follows.
\end{proof}
%
\begin{note}
  With the argumentation of the last proof,
  it also follows that the convolution of two functions of which the smoother
  one has compact support is (at least) as smooth as the smoother one.
\end{note}
%
\begin{lemma}[Support of the convolution]\label{thm:suppConvolution}
  Let $f, g ∈ \Lp{ℝ}$,
  then $\supp (f*g) ⊆ \supp (f) + \supp (g)
  \definedas \setDef{x + y}{x ∈ \supp (f), \, y ∈ \supp (g)}$.
\end{lemma}
%
\begin{proof}
  Let $x ∈ ℝ$. If there is any $z ∈ ℝ$ with $f(z) g(x - z) \neq 0$, then
  \begin{align*}
    f(z) g(x-z) \neq 0 ⇒ z ∈& \supp f ∧ x-z ∈ \supp g \\
    ⇔ z ∈& \supp f ∧ x ∈ z + \supp g \\
    ⇒ x ∈& \supp f + \supp g.
  \end{align*}
  Therefore $\cl{\supp (f*g)} ⊆ \cl{\supp f + \supp g} = \supp f + \supp g$.
\end{proof}
%
To finish the proof of $\tst$ being dense in $\Lp{ℝ}$
we need one non-trivial fact which will not be proven
here and can be read in \autocite[1.26(2)][64]{Alt:LinFuAna}.
At \autocite[2.15][115\psqq]{Alt:LinFuAna} one can also find
a detailed proof and further reading material on
dense subsets of $\LpD[p]$.
%
\begin{theorem}[Continuous compactly supported functions dense in $\LpD$]\label{thm:Ccdense}
  $\tstm 0$, the subspace of continuous functions with compact support,
  is dense in $L^p(ℝ)$ with $1 ≤ p < ∞$ with respect to the $\norm{·}_{L^p}$-norm.
\end{theorem}

\begin{theorem}[Approximation via convolution]\label{thm:ApproximationViaConvolution}
  Let $(δ_n)_{n∈ℕ}$ be a Friedrichs mollifier, $f ∈ \Lp{ℝ}$. Then
  $f * δ_n ∈ \Lp{ℝ}$ for every $n ∈ ℕ$ and
  \begin{align*}
    \lim_{n ⟶ ∞} \norm{f * δ_n - f}_{\Lp{ℝ}} = 0.		
  \end{align*}
\end{theorem}
%
\begin{proof}
  Let $(δ_n)_{n∈ℕ}$, $f$ as in the theorem.
  Then
  \begin{align*}
    \norm{f * δ_n - f}_{\Lp{ℝ}}^2
    &= ∫_{ℝ} \abs{(f * δ_n)(x) - f(x)}^2 \D x \\
    &= ∫_{ℝ} \abs {∫_{ℝ} f(z) · δ_n(x - z) \D z - f(x) · 1}^2 \D x \\
    \overset{\eqref{eq:delta_fct_int1}}&{=}
    ∫_{ℝ} \abs { ∫_{ℝ} f(z) · δ_n(x - z) \D z - f(x) · ∫_{ℝ} δ_n(x - z) \D z}^2 \D x \\
    &= ∫_{ℝ} \abs { ∫_{ℝ} (f(z) - f(x)) · δ_n(x - z) \D z}^2 \D x \\
    &≤ ∫_{ℝ} \left[∫_{ℝ} \abs{f(z) - f(x)} · δ_n(x - z) \D z\right]^2 \D x
  \end{align*}
  because of the triangle inequality.  The Cauchy-Schwarz inequality for
  \begin{equation*}
    % ⟨a, b⟩_{\Lp{ℝ}} ≤ \norm{a}_{\Lp{ℝ}} · \norm{b}_{\Lp{ℝ}}
    \text{ for }
    a = \abs{f(·) - f(x)} · δ_n(x - ·)^{\frac 12} \text{ and }
    b = δ_n(x - ·)^{\frac 12} 
  \end{equation*}
  yields the following.
  $a$ and $b$ are well-defined, since \eqref{eq:delta_fct_pos} $δ_n ≥ 0$.
  \begin{align*}
    \norm{f * δ_n - f}_{\Lp{ℝ}}^2
    &≤ ∫_{ℝ} \left[
    \left( ∫_{ℝ} \left[\abs{f(z) - f(x)} · δ_n(x - z)^{\frac 12}\right]^2 \D z \right)
  ^{\frac 12} \right. \\
  &\phantom{= ∫_{ℝ}}\left . \left( ∫_{ℝ} \left(δ_n(x-z) ^{\frac 12}\right)^2 \D z\right)
^ {\frac 12} \right] ^2 \D x\\
\overset{\eqref{eq:delta_fct_int1}, \eqref{eq:delta_fct_pos}}&{=}
∫_{ℝ} ∫_{ℝ} \abs{f(z) - f(x)}^2 · δ_n(x - z) \D z · 1^2 \D x
  \end{align*}
  With the substitution $t = z - x$ we get
  \begin{align*}
    \norm{f * δ_n - f}_{\Lp{ℝ}}^2
    &≤ ∫_{ℝ} ∫_{ℝ}\abs{f(t + x) - f(x)}^2 · δ_n(-t) \D t \D x \\
    \overset{\text{Fubini}}&{=}
    ∫_{ℝ} δ_n(-t) ∫_{ℝ} \abs{f(t + x) - f(x)}^2 \D x \D t \\
    \overset{\eqref{eq:delta_fct_supp}}&{=}
    ∫_{\left[-\frac 1n, \frac 1n\right]} δ_n (-t) \norm{f(t + ·) - f}_{\Lp{ℝ}}^2 \D t.
  \end{align*}
  The use of the Fubini's Theorem is justified because
  the last integral is finite which will be shown.

  Now Theorem \ref{thm:Ccdense} implies that there exists for any
  $ε > 0$ an $f_ε ∈ \tstm 0$ such that % a sequence
  % $(f_k)_{k ∈ ℕ}$ in
  % $\tstm 0$
  $\norm{f_ε - f}_{\Lp{ℝ}} < \frac {ε}3$.
  Since $f_ε$ has compact support 
  $\abs{\supp (f_ε(t + ·) - f_ε)}$ is bounded for small $t$
  and $f_ε$ is uniformly
  continuous. Hence, we can find a $t ∈ ℝ\without \{0\}$ such that
  $\abs{\supp (f_ε(t + ·) - f_k)}^{\frac 12} \norm{f_ε(t + ·) - f_ε}_{∞} < \frac {ε}3$.
  Then
  \begin{align*}
    \norm{f(t + ·) - f}_{\Lp{ℝ}}
    &≤ \norm{f(t + ·) - f_k(t + ·)}_{\Lp{ℝ}}
    + \norm{f_k(t + ·) - f_k}_{\Lp{ℝ}}
    + \norm{f_k - f}_{\Lp{ℝ}} \\
    &≤ \underbrace{2 \norm{f_k - f}_{\Lp{ℝ}}}_{\leq \frac {2ε}3}
    + \underbrace{\abs{\supp (f_k(t + ·) - f_k)}^{\frac 12}
    \norm{f_k(t + ·) - f_k}_{∞}}_{\leq \frac {ε}3}
    \leq ε.
  \end{align*}
  Hence $\norm{f(t + ·) - f}_{\Lp{ℝ}} → 0$ as $t → ∞$.
  Then we arrive at
  \begin{align*}
    \norm{f * δ_n - f}_{\Lp{ℝ}}^2
    &≤ ∫_{\left[-\frac 1n, \frac 1n\right]} δ_n (-t) \norm{f(t + ·) - f}_{\Lp{ℝ}}^2 \D t \\
    &≤ ∫_{\left[-\frac 1n, \frac 1n\right]} δ_n (-t) \D t
    \sup_{\abs t ≤ \frac 1n} \norm{f(t + ·) - f}_{\Lp{ℝ}}^2 \\
    &⟶ 1 · 0,\, n ⟶ ∞ \qedhere
  \end{align*}
\end{proof}

\begin{theorem}[Test functions dense in $\LpD$]\label{thm:TestfctsDense}
  $\tst$ is dense in $\Lp{ℝ}$.
\end{theorem}
\begin{proof}
  Let $f ∈ \Lp{ℝ}$.
  We have shown that $f$ can be approximated by $f * δ_n$
  with a $δ$-sequence $(δ_n)$ but $f * δ_n$ in general has no compact support.
  Therefore truncate $f$: Let
  $f_k \definedas \chFct_{[-k, k]} f$ for $k ∈ ℕ$.
  Then for all $x ∈ ℝ$,
  $\left(\chFct_{ℝ\without[-k, k]}\abs f^2\right)(x) \searrow 0$
  and therefore by monotone convergence with
  \begin{align*}
    ∞ > ∫_{ℝ} \abs f ^2
    &≥ ∫_{ℝ\without[-k, k]} \abs f^2 ≥ 0 \\
    ⇒ \norm{f - f_k}_{\Lp{ℝ}}
    &= ∫_{ℝ\without[-k, k]} \abs f^2 ⟶ 0, k ⟶ ∞
  \end{align*}
  That means, that $f_k ⟶ f$ in $\Lp{ℝ}$.
  With Theorem \ref{thm:ConvolutionInTestfcts} and \ref{thm:suppConvolution}
  $f_k * δ_n ∈ \tst$ for all $k, n ∈ ℕ$
  and $f_k * δ_n \stackrel{n ⟶ ∞}{⟶} f_k \stackrel{k ⟶ ∞}{⟶} f$ in $\Lp{ℝ}$.
  That is, what was to be shown.
\end{proof}

\docEnd
