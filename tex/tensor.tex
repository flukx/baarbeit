%! TEX program = lualatex

\input{.maindir/tex/header/preamble-section}
\docStart
\section{Tensor products of Hilbert spaces}
\label{sec:tensorproduct}

Ordinary differential equations are sometimes categorized into
scalar equations and systems of equation. The difference is that
the function searched for maps to $ℝ$ or rather $ℂ$ in our case
for scalar equations and maps to $ℂ^n$ ($n ∈ ℕ$) for systems of
equations. Explicitly calculating solutions for systems is of
course more complicated but regarding the solution theory it hardly
changes anything especially in the solution theory developed here.
That is still true when we generalise further to any Hilbert space
instead of the finite dimensional $ℂ^n$. 
One might even notice that while reading one easily overlooks that
we are talking throughout about Hilbert space valued functions
because everything works exactly like in $ℂ$. In order to get to
this point we have to properly introduce $\LpH{H}$ for an Hilbert
space $H$ though.

% In \autocite[Remark 2.9 (d), (e)]{TUD:HSDDE} the theory for complex valued functions we've seen
% so far is generalised for functions with values in any Hilbert Space, not just $ℂ$.
In order to go from the accustomed space $\LpH{ℂ}$ to $\LpH{H}$ % for some Hilbert space $H$
an equivalent construction with tensor products is used. Tensor products are not necessarily a common
topic in linear algebra courses which might be supported by the difficulty to get a visual
understanding of them. Therefore several different approaches were taken to define
and understand tensor products. Here we use conjugate bilinear forms as it is done in \autocite[][A.1]{Trostorff:Tens}.

It is easily possible to skip most of this section, especially the proofs without
any negative impact on the understanding of the rest of the
thesis since everything works out just as expected.
Important facts are:
\begin{itemize}
  \item The definitions of pure tensors \ref{def:tensor} and the tensor
    product \ref{def:tensorproduct}  as the completion of the algebraic tensor product.
  \item Operators can be lifted as expected (Definition \ref{def:AlgebraicTensorproductoflinearOperators}).
  \item $\LpH{ℂ} \tens H \isom \LpH{H}$ (Theorem \ref{thm:L2tensH=L2H})
\end{itemize}
\ref{def:tensor}.
%
\begin{definition}[Algebraic Tensor (product)]\label{def:tensor}
  Let $H_1$, $H_2$ be complex Hilbert spaces, $x ∈ H_1$, $y ∈ H_2$. Then we consider the
  conjugate bilinear continuous functional called \newTerm{pure tensor}
  \begin{align*}
    x \tens y \colon H_1 \times H_2 &\to ℂ \\
    (ϕ, ψ) &\mapsto ⟨x, ϕ⟩_{H_1} ⟨y, ψ⟩_{H_2}.
  \end{align*}
  \begin{note}[conjugate bilinear]\label{note:conjugate_bilinear}
    Here the tensors are defined to be conjugate-bilinear (or antibilinear).
    That is: $(x \tens y)(αϕ, βψ) = \conj α \conj β (x \tens y)(ϕ, ψ)$ for any
    $α, β ∈ ℂ$.
    An analogous definition could be done with bilinear functionals
    by putting $x$ and $y$ on the right side of the inner products.
    That might feel more familiar but it would also imply that the
    scalar multiplication in the tensor product would be a conjugate-linear,
    that is $α(x \tens y) = (\conj {α} x \tens y)$.
    So one has to decide which linear structure feels familiar.
    Here the linear structure in the tensor product is chosen
    because we are mostly interested in the tensor product and the actual
    definition via functionals is not so much of interest.
  \end{note} % end note conjugate bilinear
  Pure tensors are elements of the vector space of all conjugate bilinear functionals
  and therefore inherit the following linear structure.
  \begin{align*}
    \left( \sum_{i = 1}^n a_i (x_i \tens y_i) \right) (ϕ, ψ)
    = \sum_{i = 1}^n a_i (x_i \tens y_i) (ϕ, ψ)
    \quad (ϕ ∈ H_1, ψ ∈ H_2)
  \end{align*}
  for $x_i ∈ H_1$, $y_i ∈ H_2$, $a_i ∈ ℂ$ for $i ∈ \{1, …, n\}$.
  % For the case
  % $y_i = y$ or $x_i = x$ for all $i ∈ \{1, …, n\}$ this coincides with 
  % definition \ref{def:tensor} due to the
  % sesquilinearity of the inner products.

  The linear combinations are also conjugate bilinear functionals.
  We call the space of these linear combinations
  \begin{align*}
    V_1 \atens V_2 ≔ \Lin \setDef{x \tens y}{x ∈ V_1, y ∈ V_2} %\quad
    % (V_i ⊆ H_i, i ∈ \{1,2\})
  \end{align*}
  the \newTerm{algebraic tensor product} of the subsets $V_1$ of $H_1$ and $V_2$ of $H_2$.
  \begin{note}\label{note:LinearcombAreSums}
    Note that every linear combination of pure tensors can be written as
    a sum of pure tensors.
    For $n ∈ ℕ$, $α_i ∈ ℂ$, $x_i ∈ H_1$, $y_i ∈ H_2$ ($i ∈ \{1, …, n\}$):
    \begin{equation}
      Σ_{i = 1}^n α_i (x_i \tens y_i)
      = Σ_{i = 1}^n \underbrace{(α_i x_i)}_{∈ H_1} \tens y_i ∈ H_1 \atens H_2 \label{equ:LinearcombAreSums}
    \end{equation}
  \end{note} % end note LinearcombAreSums

  The inner products of $H_1$ and $H_2$ induce a sesquilinear mapping on
  the algebraic tensor product $H = H_1 \atens H_2$. We define it on pure tensors and extend it sesquilinearly:
  \begin{equation}
    \begin{split}
      ⟨x \tens y, u \tens v⟩_\tens
      & = (x \tens y) (u, v)
      = ⟨x, u⟩_{H_1} ⟨y, v⟩_{H_2}
      = \conj{⟨u, x⟩_{H_1}} \conj{⟨v, y⟩_{H_2}} \\
      & = \conj{(u \tens v)(x,y)}
      = \conj{⟨u \tens v, x \tens y⟩} \quad (x, u ∈ H_1, y, v ∈ H_2)
    \end{split}
    \label{equ:tensorsymmetry}
  \end{equation}
\end{definition} % end definition of tensor

The name \enquote{product} is justified because we get a copy of $H_2$ for every
element of $H_1$: by fixing one particular $x$ and $ϕ ∈ H_1$
one gets the trivial embedding that maps any $y ∈ H_2$ to an conjugate-linear functional on $H_2$.
Note that this space of conjugate-linear continuous functionals on an Hilbert space is
isomorphic to $H_2$ itself.

Furthermore one sees that in the case of finite dimensional $H_1$ and $H_2$,
we have $\dim (H_1 \tens H_2) = \dim (H_1) · \dim (H_2)$.

The sesquilinear form $⟨· , ·⟩_\tens$ is denoted as if it was an inner product.
\begin{lemma}\label{thm:tensorDotProd}
  For two Hilbert spaces $⟨· , ·⟩_\tens$ defines an inner product on $H_1 \atens H_2$.
\end{lemma} % end lemma
\begin{proof}
  By definition $⟨· , ·⟩_\tens$ is sesquilinear. As seen in \eqref{equ:tensorsymmetry}
  $⟨·,·⟩_\tens$ is conjugate symmetric on pure tensors.
  By sesquilinearity this extends to $H_1 \atens H_2$.

  The representation of elements of $H_1 \atens H_2$ as linear combinations of pure tensors is
  not unique. Hence it must be checked that $⟨·,·⟩_\tens$ is well-defined.
  This is true since
  $H_1 \atens H_2$ is a (linear) vector space and $⟨·,·⟩_\tens$ is defined to be sesquilinear.
  In detail,
  let
  \begin{align*}
    Σ_i^n a_i(x_i \tens y_i) &= Σ_i^m b_j (u_j \tens v_j) ∈ H_1 \tens H_2 \\
    \text{ with } n, m ∈ ℕ,
    ϕ, x_i, u_j &∈ H_1, a_i, b_j ∈ ℂ, ψ, y_i, v_j ∈ H_2
    \text{ for } j ∈ \{1, …, m\} i ∈ \{1, …, n\}. 
  \end{align*}
  Then for all $ϕ ∈ H_1$ and $ψ ∈ H_2$
  \begin{align*}
    ⟨Σ_i^n a_i(x_i \tens y_i), ϕ \tens ψ⟩_{\tens} &- ⟨Σ_i^m b_j (u_j \tens v_j), ϕ \tens ψ⟩_{\tens} \\
    &= \left(Σ_i^n a_i(x_i \tens y_i)\right) (ϕ, ψ) - \left(Σ_i^m b_j (u_j \tens v_j)\right) (ϕ, ψ) \\
    &= \left(\left(Σ_i^n a_i(x_i \tens y_i)\right) - \left(Σ_i^m b_j (u_j \tens v_j)\right)\right) (ϕ, ψ)
  \end{align*}
  By the linearity in the second argument, we have for all $w ∈ H_1 \atens H_2$
  \begin{align*}
    ⟨Σ_i^n a_i(x_i \tens y_i), w⟩_{\tens} - ⟨Σ_i^m b_j (u_j \tens v_j), w⟩_{\tens} = 0.
  \end{align*}
  This shows that $⟨·, ·⟩_{\tens}$ is a function of the first
  argument for every second argument and by conjugate symmetry $⟨·,·⟩_{\tens}$
  $⟨·, ·⟩_{\tens}$ is a function of the second argument for
  every first argument.

  For the positivity consider for $n ∈ ℕ$, $x_i ∈ H_1$, $y_i ∈ H_2$, $a_i ∈ ℂ$, $z_i = a_i x_i$ for $i ∈ \{1, …, n\}$
  \begin{align*}
    ⟨Σ_{i=1}^n a_i (x_i \tens y_i), Σ_{j=1}^n a_j (x_j \tens y_j) ⟩
    &= ⟨Σ_{i=1}^n (z_i \tens y_i), Σ_{j=1}^n (z_j \tens y_j) ⟩_{\tens} \\
    = Σ_{i=1}^nΣ_{j=1}^n ⟨z_i, z_j⟩_{H_1}⟨y_i, y_j⟩_{H_2}
  \end{align*}
  Note that the (so called \newTerm{Gramian}) matrices
  $G_1 = \left(⟨z_i, z_j⟩\right)_{i,j})$ and
  $G_2 = \left(⟨y_i, y_j⟩\right)_{i,j}$ are hermitian
  since $⟨·,·⟩_{H_1}$ and $⟨·,·⟩_{H_2}$ is conjugate symmetric.
  Hence they can be diagonalised.
  $G_1$ and $G_2$ are positive semi-definite since $⟨·,·⟩_{H_1}$ and
  $⟨·, ·⟩_{H_2}$ are positive definite.
  Positive semi-definite diagonal matrices have a square root and hence
  $G_1$ and $G_2$ have \imp{hermitian} square roots.
  Call them $A_s = \left(a_{ij}^s\right)_{i,j}$ ($s ∈ \{1,2\}$). With them
  the sum can be rewritten as (all sums going from $1$ to $n$)
  \begin{align*}
    Σ_iΣ_jΣ_{k_1} & a_{ik_1}^1a_{k_1j}^1\left(Σ_{k_2}a_{ik_2}^2{k_2j}^2\right) \\
                  &= Σ_{k_1}Σ_{k_2}\left(Σ_i a_{ik_1}^1 a_{ik_2}^2 \right) \left(Σ_j a_{k_1j}^1 a_{k_{2j}}^2\right)
                  & \text{(Rearranging sums)} \\
                  &= Σ_{k_1}Σ_{k_2}\left(Σ_i a_{ik_1}^1 a_{ik_2}^2 \right) \conj{\left(Σ_j a_{jk_1}^1 a_{jk_2}^2\right)}
                  & \text{($A_1$, $A_2$ hermitian)} \\
    %: $a_{sl}^k = \conj{(a_{ls}^k)}$}
                  &= Σ_{k_1}Σ_{k_2}\abs{Σ_i a_{ik_1}^1 a_{ik_2}^2}^2
                  & \text{($c\conj{c}=\abs c^2$, $c ∈ ℂ$)} \\
                  &≥ 0.
  \end{align*}
  To show, that $⟨·,·⟩_\tens$ is positive definite
  (not only semi-definite as we just showed), consider any
  $a ∈ H_1 \atens H_2$ with $⟨a, a⟩_\tens = 0$
  and a pure tensor $ϕ \tens ψ ∈ H_1 \atens H_2$. Then by the
  Cauchy-Schwarz-Inequality that holds for all symmetric sesquilinear forms:
  \begin{align*}
    \abs{a (ϕ, ψ)} = \abs{⟨a, ϕ \tens ψ⟩_{\tens}} ≤ √{⟨a, a⟩_{\tens}} √{⟨ϕ \tens ψ, ϕ \tens ψ⟩_{\tens}} = 0.
  \end{align*}
  Thus, $a = 0$ (as a function).
\end{proof}
%
So far everything was pure linear algebra.
We are interested in (infinite dimensional) Hilbert spaces.
Hence the question arises if the (algebraic)
tensor product of two Hilbert spaces is again complete, that is
it is an Hilbert space.
Since we only allowed finite linear combinations it might not surprise
that $H_1 \atens H_2$ is in general not an Hilbert space.
That is the reason  why we consider the completion.
%
\begin{definition}[Tensor product]\label{def:tensorproduct}
  Let $H_1$ and $H_2$ be Hilbert spaces.
  Let $H_1 \tens H_2$ be the completion of $H_1 \atens H_2$ with
  respect to the inner product $⟨·,·⟩_\tens$.
  The inner product of this Hilbert space is denoted by $⟨·,·⟩_{H_1 \tens H_2}$.
\end{definition} % end definition of tensor product
\docEnd
