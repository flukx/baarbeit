%! TEX program = lualatex

\input{.maindir/tex/header/preamble-section}
% inputs the preamble only if necessary

\docStart
\subsection{Continuous delay}
\label{sec:continuousDelay}
\begin{textfold}[intro]  
  A similar approach as for discrete delay can
  be taken to consider the second type of typical
  delay differential equations which are equations
  with continuous delay.
  The right hand side should not only depend on
  some points in the past but on the entire history.
  The most typical way of formulating this is
  (compare \autocite[Example 5.11][33]{TUD:HSDDE})
\end{textfold}
%
\begin{equation*}
  u'(t) = ∫_{- ∞}^0 h(t, θ, \past{θ}u(t)) \D θ
\end{equation*}
%
with a suitable $h \colon ℝ × ℝ_{< 0} × H ⟶ H$.
What \enquote{suitable} means we will see after
applying the solution theory.

Here $\past{}{}$ maps a function to its past:
\begin{align*}
  \past{}{} &\colon H^{ℝ} → \left(H^{ℝ_{<0}}\right)^{ℝ} \\
  \past{θ}u(t) &= u (θ + t) \quad θ ∈ ℝ_{< 0}, t ∈ ℝ
\end{align*}
In order to treat this case in the developed manner
we generalise that to the factorisation
$F = Φ ∘ Θ$ with
\begin{align*}
  Θ \colon \tst &⟶ \bigcap_{η ∈ ℝ_{> 0}} \HiH[{\LpH[ℝ_{< 0}]{H}}]{η}0 \\
  Θ \colon u &↦ \past{}{u}% \text{, that is }
  %Θ u (t, θ) = (\past{θ}u) (t) = u(t + θ) \text{ for } t ∈ ℝ, θ ∈ ℝ_{< 0}
  \\
  \text{and } Φ \colon &\bigcap_{η ∈ ℝ_{> 0}} \HiH[{\LpH[ℝ_{< 0}]{H}}]{η}0 ⟶ \dual{\tstp+} 
  % \text{where } \past{}{} \colon H^{ℝ} &→ \left(H^{ℝ_{< 0}}\right)^{ℝ} \\
  % (\past{θ}u)(t) &= u (t + θ) \text{ for } t ∈ ℝ, θ ∈ ℝ_{< 0}.
\end{align*}
%
% Note: here we see for the first time why we need the tensor product with general Hilbert spaces
%
In the example above we get for $Φ$
\begin{equation*}
  Φ(U) = \left(t ↦ \underbrace{∫_{- ∞}^0 h(t, θ, U_{θ}(t)) \D θ}_{\define g(t, U(t))}\right) 
\end{equation*}
\begin{textfold}[why norm is looked at]  
  where $g \colon ℝ \times \LpH[ℝ_{< 0}]{H} \to H$ is an intermediate step of abstraction: a function
  that takes a time and a past until this time.

  As in the previous Section \ref{sec:discreteDelay} we
  need the Lipschitz constant of $Θ$ in order to find the
  necessary Lipschitz condition for $Φ$.
  Since $Θ$ is linear, $\Lip{Θ}$ is the operator norm of Hilbert space operators.
  Let $u ∈ \tstH$.
  Then we compute:
\end{textfold}
\begin{align*}
  \norm{Θ(u)}_{\w, 0}^2 &= ∫_{ℝ} \norm{\past{}u(t)}_{\LpH[ℝ_{< 0}]{H}}^2 \exp(-2 \w t) \D t \\
                        &= ∫_{ℝ} ∫_{ℝ_{< 0}} \norm{u (t + θ)}_H^2 \D θ \exp(-2 \w t) \D t \\
                        &= ∫_{ℝ_{< 0}} ∫_{ℝ} \norm{u (t + θ)}_H^2 \exp(- 2 \w t) \D t \D θ & \text{Fubini} \\
                        &= ∫_{ℝ_{< 0}} ∫_{ℝ} \norm{u (s)}_H^2 \exp(-2 \w s) \D s \exp(2 \w θ) \D θ & s = t + θ \\
                        &= ∫_{ℝ_{< 0}} \norm{u}_{\w, 0}^2 \exp(2 \w θ) \D θ \\
                        &= \norm u_{\w, 0}^2 ∫_{ℝ_{< 0}} \exp(2 \w θ) \D θ \\
                        &= \norm u_{\w, 0}^2 \left( \left . \frac 1{2 \w} \exp(2 \w θ) \right |_{- ∞}^0\right)
  = \frac 1{2 \w} \norm u_{\w, 0}^2 \\
  ⇒ \Lip{Θ} &= \frac 1{√{2 \w}}
\end{align*}
The application of the theorem of Fubini is permitted since
the double integral is finite as shown and all integrands are non-negative.

Hence with growing $\w$ the Lipschitz constant of $Φ$ can grow
as well. So we get to the following conclusion.
%
\begin{theorem}[Continuous delay, {\autocite[Theorem~5.10][32]{TUD:HSDDE}}] \label{thm:ContinuousDelay}
  Let
  \begin{equation*}
    Φ \colon \bigcap_{η ∈ ℝ_{> 0}} \HiH[{\LpH[ℝ_{< 0}]H}]{η}0 ⟶ \dual{\tstp+}
  \end{equation*}
  such that there exists $\w_0$ such that for all $\w ∈ ℝ_{> \w_0}$,
  there exists $K ∈ ℝ_{> 0}$ and $s < √{2 \w}$ such that
  for all $u, w ∈ \bigcap_{η ∈ ℝ_{> 0}} \HiH[{\LpH[ℝ_{< 0}H]}]{η}0$ and
  $ψ ∈ \tstp+$ we have
  %
  \begin{equation*}
    \abs{Φ(0)(ψ)} ≤ K \norm{ψ}_{- \w, 1} \text{ and }
    \abs{Φ(u)(ψ)-Φ(w)(ψ)} ≤ s \norm{ψ}_{-\w, 1} \norm{u - w}_{\w, 0}.
  \end{equation*}
  %
  Let $Φ_{\w}$ be the Lipschitz continuous extension of $Φ$ to a mapping
  from $\HiH[{\LpH[ℝ_{< 0}]H}]{\w}0$ to $\HiH{\w}{-1}$.
  Then the equation
  \begin{equation*}
    \der u = Φ_{\w}(\past{}u)
  \end{equation*}
  has a unique solution and the solution operator is causal.

  \begin{note}[Possible Lipschitz constant]\label{note:Possible_Lipschitz_constant}
    One possibility to describe $s$ depending on $\w$ is
    $C \w^{r}$ with $r ∈ (0, \frac 12)$ and $C ∈ ℝ_{> 0}$.
    In order to have $C \w^{r} < √{2 \w}$ calculate:
    \begin{align*}
      C \w^{r} < (2 \w)^{\frac 12}
      ⇔ \frac C{√2} < \w^{- r + \frac 12}
      ⇔ \w > \left(\frac C{√2}\right)^{\frac 1{\frac 12 - r}} = \left(\frac C{√2}\right)^{\frac 2{1 - 2r}}. \\
    \end{align*}
    So choose $\w_0 > \left(\frac C{√2}\right)^{\frac 2{r - 2}}$.
  \end{note} % end note Possible Lipschitz constant
\end{theorem} % end theorem Continuous delay
% proof done beforehand
\begin{example}[{\autocite[Example~5.11~(a)][33]{TUD:HSDDE}}]
  Going back to the beginning of the section we should ask what
  the condition for $Φ$ means for $g$.

  The first one:
  \begin{align*}
    \abs{Φ(0)(ψ)} &≤ K \norm{ψ}_{- \w,1} \\
    \text{implies for $g$: } \abs{∫_{ℝ} ⟨g(t, 0), ψ(t)⟩_H \D t} &= \abs{g(·, 0) (ψ)} ≤ K \norm{ψ}_{- \w, 1} \\
    \text{where } \abs{∫_{ℝ} ⟨g(t, 0), ψ(t)⟩_H \D t}
    &= \abs{∫_{ℝ} ⟨g(t, 0), \der[-\w]^{-1} \der[-\w] ψ(t)⟩_H \D t} \\
    \overset{\ref{eq:duality_derivative}}&= \abs{∫_{ℝ} ⟨\der^{-1} g(t, 0), \der[-\w] ψ(t)⟩_H \D t} \\
    &≤ \norm{\der^{-1} g(·, 0)}_{\w, 0} \norm{\der[-\w]ψ(t)}_{-\w, 0} \\
    %&\overset{\ref{thm:derivative_unitary}}&= \norm{g(·, 0)}_{\w, -1} \norm{ψ}_{-\w, 1}
    \overset{\ref{thm:diffContinuouslyInvertible}}&{≤} \frac 1{\w} \norm{g(·, 0)}_{\w, 0} \norm{ψ}_{-\w, 1}
  \end{align*}
  Hence we need to impose on $g$, that there is a $\w_0 ∈ ℝ_{>0}$ such that for all $\w ∈ ℝ_{> \w_0}$
  $\norm{g(·, 0)}_{\w, 0}$ is finite.

  Similarly we can formulate the Lipschitz condition without the test function and
  in $\HiH{\w}0$. Let $u, w ∈ \bigcap_{η ∈ ℝ_{> 0}} \HiH[{\LpH[ℝ_{<0}]H}]{η}0 $.
  \begin{align*}
    \norm{t ↦ g(t, u(t)) - g (t, w(t))}_{\w, 0} ≤ \w s \norm{u - w}_{\w, 0}
  \end{align*}
  where the extra $\w$ comes from the step from $\HiH{\w}{-1}$ to $\HiH{\w}0$.
  Written with integrals, this is
  \begin{align*}
    ∫_{ℝ} \norm{g(t, u(t)) - g(t, w(t))}_H^2 \exp(-2 \w t)\D t ≤ \w^2 s^2 ∫_{ℝ} \norm{u(t)-w(t)}_H^2 \exp(-2 \w t) \D t
  \end{align*}
  One option for $g$ to fulfill this estimate is a Lipschitz constant
  $L ∈ ℝ_{> 0}$ such that
  we have for all $x, y ∈ \LpH[ℝ_{>0}]H$ and all $t ∈ ℝ$
  \begin{equation*}
    \norm{g(t, x) - g(t, y)}_H ≤ L \norm{x - y}_{\LpH[ℝ_{> 0}]H}
  \end{equation*}
  which would imply
  \begin{align*}
    ∫_{ℝ} \norm{g(t, u(t)) - g(t, w(t))}_H^2 \exp(-2 \w t)\D t
    &≤ ∫_{ℝ} L \norm{u(t) - w(t)}_H^2 \exp(-2 \w t)\D t \\
    &= L \norm{u - w}_{\w, 0}^2.
  \end{align*}
  This $L$ must therefore fulfill
  \begin{align*}
    L ≤ \w^2 s^2 < \w^2 · 2\w
  \end{align*}
  for sufficiently large $\w$. Since $2 \w^3 ⟶ ∞$, $L$ can be chosen arbitrary.

  In conclusion we have the following result.
\end{example}
\begin{corollary}\label{thm:continuousDelayg}
  Let $g \colon ℝ \times \LpD(ℝ_{<0}; H) → H$ such that there
  exists $L ∈ ℝ_{> 0}$ such that for all sufficiently large
  $\w$ and all $t ∈ ℝ$
  and $x, y ∈ \LpH[ℝ_{< 0}]H$ 
  \begin{equation*}
    \norm{g(·, 0)}_{\w, 0} \text{ is finite and } \norm{g(t, x) - g(t, y)}_H ≤ L \norm{x - y}_{\LpH[ℝ_{< 0}]H}.
  \end{equation*}
  Then the equation
  \begin{equation*}
    \der u (t) = g(t, \past{}u(t)) \quad (t ∈ ℝ)
  \end{equation*}
  has a unique solution and the solution operator is causal.
\end{corollary} % end corollary
If we now plug in $h$ into the definition of $g$ and
look for suitable conditions on $h$ the result is
very bulky. So we go back to the beginning at the
condition for $F$ and assume $h$ to be Lipschitz
continuous with respect to the third argument.
\begin{corollary}[{\autocite[Example~5.11~(b)][33]{TUD:HSDDE}}] \label{thm:continuousDelayh}
  Let $\w_0 ∈ ℝ_{> 0}$,
  \begin{equation}
    \label{eq:h_condition}
    \begin{split}
      h \colon ℝ \times ℝ_{< 0} \times H &→ H \\
      \text{with } h(t, θ, 0) &= 0 \\
      \text{and } \norm{h(t, θ, x) - h(t, θ, y)}_H &\leq L(θ) \norm{x - y}_H
      \text{ for all } t ∈ ℝ, θ ∈ ℝ_{< 0}, x, y ∈ H \\
      \text{with } L\colon ℝ_{<0} &→ ℝ_{>0} \\ \text{ such that}
      ∫_{- ∞}^0 L(θ) \exp(\w θ) \D θ &< \w %\tilde L \text{ for some } \tilde L ∈ ℝ_{> 0}
      \text{ for all } \w \geq \w_0.
    \end{split}
  \end{equation}
  For example $L$ can be any constant function.
  Let
  \begin{align*}
    F \colon \tstH &→ \Isect_{η ∈ ℝ_{> \w_0}} \HiH{η}0 \subset \dual{\tstH} \\
    F(u) &= \left(t ↦ ∫_{- ∞}^0 h(t, θ, \past{θ}u(t)) \D θ\right).
  \end{align*}
  For all $\w \geq \w_0$ let $F_{\w}$ be the Lipschitz continuous extension
  of $F$ to a mapping from $\HiH{\w}0$ to $\HiH{\w}{-1}$.
  Then the equation
  \begin{equation}
    \label{eq:hequation}
    \der u = F_{\w}u
  \end{equation}
  has a unique solution and the solution operator is causal.
\end{corollary} % end corollary name
<<<<<<< HEAD
\begin{proof}  
  Let $\w ∈ ℝ_{\geq \w_0}$,
  $u, v ∈ \tstH$, $ψ ∈ \tstp+$.
  $F_{\w}$ maps into $\HiH{\w}{-1}$ since
  \begin{align*}
    \abs{F(0)(ψ)}
    = \abs{∫_{ℝ} ⟨∫_{-∞}^0 h(t, θ, 0) \D θ, ψ(t) ⟩_H \D t}
    = \abs{⟨0, ψ⟩_{0,0}}
    = 0.
  \end{align*}
  Check the Lipschitz condition for $F$.
  \begin{align*}
    &\hspace{-0.5cm}\abs{F(u)(ψ)-F(v)(ψ)} \\
    &= \abs{∫_{ℝ} ⟨∫_{-∞}^0 h (t, θ, \past{θ}u(t)) \D {θ}, ψ(t) ⟩_H- ⟨∫_{-∞}^0 h (t, θ, \past{θ}v(t)) \D {θ}, ψ(t) ⟩_H \D t } \\
    &= \abs{∫_{ℝ} ∫_{-∞}^0 ⟨ h(t, θ, u(t + θ)) - h(t, θ, v(t + θ)), ψ(t) ⟩_H \D{θ} \D t } \\
    \intertext{With Fubini this is}
    &= \abs{∫_{-∞}^0 ∫_{ℝ}⟨ h(t, θ, u(t + θ)) - h(t, θ, v(t + θ)), ψ(t) ⟩_H \D t \D{θ}} \\
    % & \qquad\text{(Fubini)} \\
    &= \abs{∫_{-∞}^0 ⟨ h(·, θ, u(· + θ)) - h(·, θ, v(· + θ)), ψ ⟩_{0, 0} \D{θ}} \\
    \intertext{By \eqref{eq:duality_derivative} and Cauchy-Schwarz inequality this can be estimated by}
    &\leq ∫_{-∞}^0 \norm{t ↦ h(t, θ, u(t + θ)) - h(t, θ, v(t + θ))}_{\w, -1}
    \norm{ψ}_{-\w, 1} \D {θ} \\
    \intertext{By Corollary \ref{thm:diffContinuouslyInvertible} estimate this with}
    &\leq \frac1{\w} \norm{ψ}_{-\w, 1} ∫_{-∞}^0 \norm{t ↦ h(t, θ, u(t + θ)) - h(t, θ, v(t + θ))}_{\w, 0} \D {θ} \\
    &= \frac1{\w} \norm{ψ}_{-\w, 1} ∫_{-∞}^0 \left( ∫_{ℝ} \norm{h(t, θ, u(t + θ)) - h(t, θ, v(t + θ))}_H^2\exp(-2 \w t) \D t\right)^{\frac 12} \D {θ} \\
    &\leq \frac1{\w} \norm{ψ}_{-\w, 1} ∫_{-∞}^0 \left(∫_{ℝ} L(θ)^2\norm{u(t + θ) - v(t + θ)}_H^2  \exp(-2 \w t) \D t \right)^{\frac 12} \D {θ} \\
    \intertext{With the substitution $s = t + θ$ this is}
    &= \frac1{\w} \norm{ψ}_{-\w, 1} ∫_{-∞}^0 \left(∫_{ℝ} L(θ)^2 \norm{u(s) - v(s)}_H^2 \exp(-2\w(s - θ)) \D s \right)^{\frac 12} \D {θ} \\
    &= \frac1{\w} \norm{ψ}_{-\w, 1} ∫_{-∞}^0 L(θ) \norm{u - v}_{\w, 0} \exp(\w θ) \D {θ} \\
    &< \frac1{\w} \norm{ψ}_{-\w, 1} \w \norm{u - v}_{\w, 0}
    = \norm{ψ}_{-\w, 1} \norm{u - v}_{\w, 0}
    % \left[\exp(\w θ)\frac1{θ}\right]_{-∞}^0 \\
    % &= \frac {\tilde L}{\w} \norm{ψ}_{-\w, 1} \norm{u-v}_{\w, 0}
  \end{align*}
  By Theorem \ref{thm:Picard-Lindelöf} of Picard-Lindelőf
  \eqref{eq:hequation} has a unique solution
  for all $\w > \w_0$ and Theorem \ref{thm:solutionweightindependent}
  implies that the solution does not depend on $\w$.
  By Theorem \ref{thm:Causalityofsolving} the solution operator is causal.
\end{proof}
\docEnd
