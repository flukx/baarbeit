%! TEX program = lualatex

\input{.maindir/tex/header/preamble-section}

\docStart

\section{Closed and closable operators}\label{sec:Close}
% mostly summarized from \autocite[][]{DWerner:FuAna}, ch. IV.4
%
This section summarizes some results from \autocite[IV.4, VII.2][343\psqq]{DWerner:FuAna}
that we will use to work with the differentiation operator $\deriv$, which
will be introduced later in Section \ref{sec:closure_of_the_time_derivative}.
\begin{definition}[Closed operator, {\autocite[IV.4.1][156]{DWerner:FuAna}}]\label{def:closedOperator}
  Let $X$ and $Y$ be normed spaces,
  $D \subseteq X$ a subspace,
  $T\colon X \supseteq D \to Y$ linear.
  Then $T$ is called \newTerm{closed} if for every convergent sequence
  $(x_n)_{n ∈ ℕ}$, in $D$, $x_n \to x ∈ X$ with $Tx_n \to y \in Y$, we have $x \in D$ and $Tx = y$.
\end{definition}
%
Note that this is weaker than continuity.
Closedness of an operator $T$ can also be viewed as closedness of its graph
$\gr(T) = \setDef{(x, Tx)}{x ∈ D} \subseteq X \times Y$
(under the norm $\normiii {(x, y)} = \norm x + \norm y$) as one can check.
(See \autocite[IV.4.2][156\psq]{DWerner:FuAna}.)

\begin{definition}[Closable operator, Closure, {\autocite[VII.2.1][343]{DWerner:FuAna}}] \label{def:Closable}
  % from http://mathworld.wolfram.com/ClosableOperator.html
  An operator
  $T ⁚X ⊇ \dom(T) ⟶ Y$
  is called \newTerm{closable} if there exists a closed extension $B$ of $T$,
  that is $B ⁚ X ⊇ \dom(B) ⟶ Y$, such that $B$ is closed and $\dom(T) ⊆ \dom(B)$ and
  $\rest{B}{\dom(T)} = T$. If $B$ is the smallest closed extension,
  it is called the \newTerm{closure} of $T$ and $\gr(B) = \cl {\gr(T)}$ (closure in $X × Y$).
  Hence we write $B = \cl T$ for the closure.

  We write $T \subseteq S$ for two operators
  $T \colon X \supseteq \dom(T) → Y$, $S \colon X \supseteq \dom(S) → Y$
  if $\gr(T) \subseteq \gr(S)$. That is, $\dom(T) \subseteq \dom(S)$ and
  $\rest{S}{\dom(T)} = T$.
\end{definition}

From now on in this chapter we only consider operators on Hilbert spaces.

Closely connected to the concept of closed operators are adjoint operators.
For continuous linear mappings $A \colon H → H$ on Hilbert spaces there
exist continuous adjoint operators $A^*$
that satisfy
\begin{equation*}
  ⟨Ax, y⟩ = ⟨x, A^*y⟩ \text{for all } x, y ∈ H.
\end{equation*}
In the case of (unbounded) operators the adjoint operator can still be
defined but one has to be careful about the domain.

As a preparation we look at densely defined continuous operators first.
\begin{lemma}[Continuous extension of continuous operators] \label{thm:contExtension}
  Let $X, Y$ be Hilbert spaces and
  $T ⁚ X ⊇ \dom(T) ⟶ Y$ be a densely defined, continuous operator.
  Then there is a unique extension of $T$ to a continuous operator
  $\cl {T}: X ⟶ Y$ (which is automatically the closure of $T$).
\end{lemma}
\begin{proof}
  In order to show that $T$ can be continuously extended to $X$,
  we want to show that for all $\hat x ∈ X$ and all sequences
  $(x_i)_{i ∈ ℕ}$ in $\dom(T)$ that converge to $\hat x$, the limit $\lim_{i ⟶ ∞} Tx_i$
  exists and is independent of the choice of the $x_i$. Then
  \begin{align*}
    \cl {T}\hat x \definedas \lim_{i ⟶ ∞} Tx_i
  \end{align*}
  is well-defined and $\cl{T}$ is the only continuous extension of $T$.  

  \paragraph{Existence of the limit and continuity}
  As $(x_i)_{i ∈ ℕ}$ is a Cauchy sequence and $T$ is continuous,
  $(Tx_i)_{i ∈ ℕ}$ is a Cauchy sequence as well:
  $\norm{Tx_i - Tx_k}_Y = \norm{T(x_i - x_k)}_Y \leq \norm{T}_{L(X, Y)} \norm{x_i - x_k}$.
  Now, $Y$ is a Hilbert space, so $(Tx_i)_{i ∈ ℕ}$ converges to some $\hat y∈ Y$
  and by the limit definition of $\cl T$,
  $\norm y_Y = \norm{\cl T \hat x}_Y \leq \norm T_{L(X, Y)} \norm{\hat x}_X$.
  That means, that $\cl T$ is continuous with the same operator norm as $T$.

  \paragraph{Uniqueness} Let $(x_i)_{i ∈ ℕ}$ and $(u_i)_{i ∈ ℕ}$
  be two sequences in $\dom(T)$, both converging to $\hat x∈ X$,
  then $(x_i - u_i)_{i ∈ ℕ}$ converges to $0$
  and hence so does $(Tx_i - Tu_i)_{i ∈ ℕ} = (T(x_i - u_i))_{i ∈ ℕ}$,
  because $T$ is continuous.
  This means $ \lim_{i ⟶ ∞} Tx_i = \lim_{i ⟶ ∞} Tu_i$,
  so $\cl{T}$ is well-defined.

  \paragraph{Linearity}
  Let $\hat x, \hat u ∈ X$, $λ ∈ ℂ$ and
  and $(x_i)_{i ∈ ℕ}$ and $(u_i)_{i ∈ ℕ}$ be sequences in $\dom(T)$
  converging to $\hat x$ and $\hat u$, respectively. Then
  $x_i + λ u_i → x + λ u$ and therefore
  \begin{align*}
    \cl{T} (\hat x + λ \hat u)
    &= \lim_{i ⟶ ∞} T(x_i + λ u_i) = \lim_{i ⟶ ∞} (Tx_i + λ Tu_i) \\
    &= \left(\lim_{i ⟶ ∞} Tx_i\right) + λ \left(\lim_{i ⟶ ∞} Tu_i\right)
    = T\hat x + λ T\hat u . \qedhere
  \end{align*}
\end{proof}
%
\begin{lemma}[Well-definedness of the adjoint operator, {\autocite[VII.2][344]{DWerner:FuAna}}] \label{thm:defadjoint}
  Let $X, Y$ be Hilbert spaces and $T ⁚ X ⊇ \dom(T) ⟶ Y$ be a densely defined operator.
  Then consider
  %
  \begin{align*}
    \dom(T^*) \definedas \setDef{y ∈ Y}{x ↦ ⟨Tx, y⟩_Y \text{ continuous on } \dom(T)}.
  \end{align*}
  %
  $\dom(T^*)$ is a linear subspace and for $y ∈ \dom(T^*)$
  we can extend $f \colon x ↦ ⟨Tx, y⟩_Y$
  to a unique continuous linear functional on $X$.
  Furthermore, there is a unique $z ∈ X$ such that $⟨Tx, y⟩_Y = ⟨x, z⟩_X$
  for all $x ∈ \dom(T)$.
  We write $T^*y \definedas z$.
  $T^*\colon Y \supseteq \dom(T^*) → X$ is a linear operator.
\end{lemma}
%
\begin{proof}
  Since $⟨·, ·⟩_Y$ is sesquilinear, $\dom(T^*)$ is a linear subspace of $Y$.
  Let $y ∈ \dom(T^*)$. Then the mapping
  $f \colon X \supseteq \dom(T) → ℂ$ defined by $f(x) = ⟨Tx, y⟩_Y$
  is by definition densely defined, linear and continuous.
  By Lemma \ref{thm:contExtension} $f$ has a unique continuous
  extension to $X$.

  By the Fréchet-Riesz representation theorem
  (see \autocite[Theorem V.3.6][228]{DWerner:FuAna})
  there is a unique $z ∈ X$ such that
  $⟨Tx, y⟩_Y = ⟨x, z⟩_X$ for all $x ∈ \dom(T)$.

  The linearity of $T^*$ follows from the uniqueness of $z$ in
  the equation above,
  the sesquilinearity of the inner product and the linearity
  of $T$.
\end{proof}
%
\begin{definition}[Adjoint operator, Self-adjoint, {\autocite[VII.2.3][344]{DWerner:FuAna}}] \label{def:adjoint}
  The operator described in Lemma \ref{thm:defadjoint}
  %
  \begin{align*}
    T^* ⁚ Y \supseteq \dom(T^*) ⟶ X
  \end{align*}
  with
  \begin{align*}
    ⟨Tx, y⟩ = ⟨x, T^*y⟩ \quad \text{for all } x ∈ \dom(T), y ∈ \dom(T^*)
  \end{align*}
  %
  is the \newTerm{adjoint operator} of $T$.

  If $T = T^*$ (which implies $\dom(T) = \dom(T^*)$ and $X = Y$), we call
  $T$ \newTerm{self-adjoint}.
  $T$ is called \newTerm{skew-self-adjoint} if $\im T$ is self-adjoint.

  For a complex number $z ∈ ℂ$, $\conj z$ is the complex conjugate.
  This notation makes sense,
  since $\scaProd {zx, y} = \scaProd {x, z^*y}$ for $x, y$ elements of
  a complex Hilbert space.
\end{definition}
%
The adjoint operator has nice properties. First of all, it is closed:
\begin{lemma}[Closedness of $T^*$, {\autocite[VII.2.4 (a)][345]{DWerner:FuAna}}] \label{thm:adjointClosed}
  Let $X, Y$ be Hilbert spaces and $T ⁚ X ⊇ \dom(T) ⟶ Y$ be a densely defined operator.
  Then the adjoint operator $T^*$ is closed.
\end{lemma}
\begin{proof}
  Let $(y_n)_{n ∈ ℕ}$ be a sequence in $\dom(T^*)$ with $y_n ⟶ y ∈ Y$ and $T^* y_n ⟶ \hat x ∈ X$ for $n ⟶ ∞$.
  Then for $x ∈ \dom(T)$
  %
  \begin{align*}
    ⟨Tx, y⟩ &\overset{(*)}&= \lim_{n ⟶ ∞} ⟨Tx, y_n⟩ = \lim_{n ⟶ ∞} ⟨x, T^* y_n⟩\\
            &= ⟨x, \hat x⟩ \overset{(*)}&{⇒} x ↦ ⟨Tx, y⟩ \text { is continuous}
  \end{align*}
  %
  where in $(*)$ we have used the continuity of $⟨·,·⟩$.
  This means $y ∈ \dom(T^*)$ and $T^*y = \hat x$,
  as shown in Lemma \ref{thm:defadjoint}.
\end{proof}
%
Secondly, the adjoint of an operator is also the adjoint of the closure:
\begin{lemma} \label{thm:adjointOfClosure}
  Let $X, Y$ be Hilbert spaces and $T ⁚ X ⊇ \dom(T) ⟶ Y$ be a densely defined closable
  operator.
  Then $T^* = \left(\cl{T}\right)^*$.
\end{lemma} % end lemma  of an is the same as the adjoint of the 
\begin{proof}
  Let $y ∈ \dom\left(\left(\cl{T}\right)^*\right)$. Then $x ↦ ⟨\cl T x, y⟩$ is continuous
  on $\dom(\cl T)$ and in particular on $\dom(T) \subseteq \dom(\cl{T})$.
  So $y ∈ \dom(T^*)$ and since $\dom(T)$ is dense in $X$, $T^*y = \left(\cl{T}\right)^*y$.

  Let $y ∈ \dom(T^*)$ and let $x ∈ \dom(\cl T)$. Then there is a sequence
  $(x_n)_{n ∈ ℕ}$ in $\dom(T)$ with $x_n → x$ and $T x_n → T x$.
  Hence
  \begin{equation*} 
    \abs{⟨\cl T x, y⟩} = \abs{\lim_{n → ∞} ⟨T x_n, y⟩}
    = \abs{\lim_{n → ∞} ⟨x_n, T^* y⟩}
    \leq \lim_{n → ∞} \norm{x_n}_X \norm{T^* y}_Y = \norm{x}_X \norm{T^* y}_Y.
  \end{equation*}
  So $x ↦ ⟨\cl T x, y⟩$ is bounded by $\norm{T^* y}_Y$
  and hence $y ∈ \left(\cl T\right)^*$
  and $T^* y = \left(\cl T\right)^* y$.
\end{proof}
%
Thirdly, the closure can be characterised by applying $^*$ twice:
\begin{lemma}[Characterisation of closability {\autocite[Theorem 1.8][3]{Loss:closableOp}}] % {\autocite[VII.2.4 (b), (c)][345]{DWerner:FuAna}}, (similar statement)
  \label{thm:closability}
  Let $X, Y$ be Hilbert spaces and $T ⁚ X ⊇ \dom(T) ⟶ Y$ be a densely defined operator.
  Then $\dom(T^*)$ is dense in $Y$ if and only if $T$ is closable.
  Furthermore, in this case $T^{**} = \cl T$ is the closure of $T$.
\end{lemma}
%
\begin{proof}
  For the easy direction assume that $T^*$ has dense domain.
  Then $T^{**}$ exists and
  for every $x ∈ \dom(T)$, $y ∈ \dom(T^{*})$, we have $⟨x, T^*y⟩ = ⟨Tx, y⟩$,
  thus $y ↦ ⟨T^*y, x⟩ = ⟨y, Tx⟩$ is continuous which means that $x ∈ \dom(T^{**})$ and $T^{**}x = Tx$.

  So $T⊆T^{**}$ and $T^{**}$
  is closed by the Lemma~\ref{thm:adjointClosed},
  hence $T^{**}$ is a closed extension of $T$.

  The other direction is more complicated. Assume that $T$ is closable.
  Since $T^* = \left(\cl T\right)^*$ by Lemma \ref{thm:adjointOfClosure}
  assume without loss of generality that $T$ is closed.
  It is to be shown that $\dom(T^*)$ is dense in $Y$. Take any $f ∈ \dom(T^*)^{\perp}$.
  For this consider the minimization problem
  %
  \begin{align*}
    M = \inf_{g ∈ \dom(T)} \norm{f - Tg}^2 + \norm g ^2.
  \end{align*}
  %
  (Note that $M = 0$ if $f = 0$ which is what we want to show.)
  % implies that $T$ is not closable since
  % there would be a sequence $(g_n)_{n ∈ ℕ}$ in $\dom(T)$
  % such that $g_n ⟶ 0$ but $T g_n ⟶ f \neq 0$.)
  %existence of such a sequence would imply $M = 0$.)

  The space $X \times Y$ with the inner product $⟨(g_1, f_1), (g_2, f_2)⟩ ≔ ⟨g_1, g_2⟩ + ⟨f_1, f_2⟩$
  is an Hilbert space and $\gr(T)$ is a closed subspace.
  By the projection theorem illustrated in Figure \ref{fig:projectionTheorem}
  this infimum is attained by an
  $(h, Th) ∈ \gr(T)$ such that
  \begin{gather*}
    M = \norm{(0, f) - (h, Th)}^2 = \norm h^2 + \norm{f - Th}^2 \\
    \text{and } ⟨(v, Tv), (h, Th) - (0, f)⟩_{X \times Y} = 0 \text{ for all } (v, Tv) ∈ \gr(T) \\
    \text{which implies } ⟨v, h⟩_X = ⟨Tv, f - Th⟩_Y \text{ for all } v ∈ \dom(T).
  \end{gather*}
  \begin{figure}
    \centering
    \tikzinput{projectionTheorem}
    \caption{Projection theorem in $X \times Y$, see \autocite[Theorem~2.2 with Note~2.3~⟨2⟩][314\psq]{Alt:LinFuAna}}
    \label{fig:projectionTheorem}
  \end{figure}
  %
  This is the definition for $T^*(f - Th)$, that is
  \begin{equation}\label{eq:AdjointOfh}
    f - Th ∈ \dom(T^*) \text{ and } T^*(f - Th) = h.
  \end{equation}
  Since $f \perp \dom(T^*)$, we can conclude with the Cauchy-Schwarz inequality
  %
  \begin{equation} \label{eq:f<Th}
    ⟨f, f - Th⟩ = 0 ⇒ \norm f^2 = ⟨f, f⟩ = ⟨f, Th⟩ ≤ \norm f \norm {Th} ⇒ \norm f ≤ \norm {Th}.
  \end{equation}
  %
  Further
  %
  \begin{align*}
    \norm h^2 =
    &⟨h, h⟩ \stackrel{\eqref{eq:AdjointOfh}}=
    ⟨h, T^*(f - Th)⟩ =
    ⟨Th, f - Th⟩ =
    ⟨Th, f⟩ - \norm {Th}^2 =
    \norm f^2 - \norm{Th}^2 \\
    ⇒ &\norm h^2 + \norm{Th}^2 = \norm {f}^2 \stackrel{\eqref{eq:f<Th}}{≤} \norm {Th}^2 \\
    ⇒ &\norm h^2 ≤ 0
    ⇒ h = 0 ⇒ Th = 0 \\
    \stackrel{\eqref{eq:AdjointOfh}}{⇒} &f-Th = f ∈ \dom(T^*) ∩ \dom(T^*)^\perp ⇒ f = 0.
  \end{align*}
  This shows that $\dom(T^*)$ is dense in $Y$.

  Now we have $\cl T ⊆ T^{**}$ and want to show $T^{**} ⊆ \cl T$
  which means by definition that
  $\gr (T^{**}) ⊆ \cl {\gr(T)} ⊆ X × Y$
  as mentioned in Definition \ref{def:Closable}.

  As before we use the inner product $⟨ (u,v), (x,y) ⟩_{X \times Y} \definedas ⟨ u,x ⟩_X + ⟨ v,y ⟩_Y$ on $X \times Y$.
  For $\cl {\gr(T)} ⊇ \gr (T^{**})$ it is enough to show that
  $ {\gr(T)}^{\perp} ⊆ \gr (T^{**})^{\perp}$.
  So let $(u,v) ∈ \gr (T)^{\perp}$,
  i.\,e.\ $⟨x,u⟩_X + ⟨Tx, v⟩_Y = 0$ for all $x ∈ \dom(T)$.
  Then $x ↦ ⟨Tx, v⟩ = ⟨x, -u⟩$ is continuous,
  hence $v ∈ \dom(T^*)$ and $T^* v = -u$.
  For $(z, T^{**}z) ∈ \gr(T^{**})$ we then have
  %
  \begin{align*}
    ⟨(z, T^{**}z), (u,v)⟩_{X \times Y} &= ⟨z, u⟩_X + ⟨T^{**} z, v⟩_Y
    = ⟨z,u⟩_X + ⟨z, T^*v⟩_X \\
    &= ⟨z, u + T^* v ⟩_X = ⟨z, 0⟩_X = 0 \\
    &⇒ (u, v) \perp {\gr(T^{**})}^{\perp}
  \end{align*}
  %
  This is what we wanted to show.
\end{proof}
In the following we consider operators from one Hilbert space to itself and therefore call this Hilbert space $H$.

An important class of operators are the symmetric ones:
\begin{definition}[Symmetric operator, {\autocite[VII.2.2][344]{DWerner:FuAna}}] \label{def:symmOp}
  Let $T ⁚ H \supset \dom(T) ⟶ H$ be a densely defined operator with
  $⟨Tx, y⟩ = ⟨x, Ty⟩$ for all $x,y ∈ \dom(T)$.
  Then $T$ is called \newTerm{symmetric}.
\end{definition}
%
This definition of symmetry appears to be almost the same as self-adjoint but
the domains (for $T = T^*$, $\dom(T) = \dom(T^*)$ is required)
are different.
That is why those two terms should not be confused.
(Also see \autocite[Ch.~9][169\psqq]{Hall:2009}.)
%
\begin{lemma}[Symmetric operators are closable, {\autocite[Proposition~9.4][171]{Hall:2009}}] \label{thm:ClosureOfSymmetricInAdjoint}
  Let $T ⁚ H ⊃ \dom(T) ⟶ H$ be a symmetric operator.
  Then $T$ is closable with $\cl T ⊆ T^*$.
\end{lemma}
%
\begin{proof}
  Let $y ∈ \dom(T)$.
  Then $x ↦ ⟨Tx, y⟩ = ⟨x, Ty⟩$ is clearly continuous,
  since $⟨·,·⟩$ is continuous and for all $x ∈ \dom(T)$,
  $⟨x, Ty⟩ = ⟨Tx, y⟩ = ⟨x, T^*y⟩$ holds.
  Hence $T ⊆ T^*$.
  In every metric space we have for two sets $A ⊆ B ⇒ \cl A ⊆ \cl B$,
  so
  $\cl {\gr (T)}
  ⊆ \cl {\gr (T^*)}
  \stackrel{\ref{thm:adjointClosed}}= \gr (\cl {T^*})
  = \gr (T^*)$
  and with Definition \ref{def:Closable} this is
  $\cl T ⊆ T^*$.
\end{proof}

\docEnd
