%! TEX program = lualatex

\input{.maindir/tex/header/preamble-section}
% inputs the preamble only if necessary

\docStart
%
\section{Solution Theory}
\label{sec:basic_solution_theory}
\begin{textfold}[intro]
  After introducing differentiation as an operator between different function spaces
  we now use this setting to tackle (ordinary) differential equations.
  Usually when a differential equation arises in any application it is not
  given which properties a solution must have, i.\,e.\ in which space a solution is to
  be found. This gives the opportunity to choose a function space in which
  a solution is searched for.

  An ordinary differential equation can be written in the form
  \begin{equation*}
    x'(t) = f(x(t), t) \quad t ∈ ℝ
  \end{equation*}
  with $f \colon H × ℝ ⟶ ℝ$. In order to simplify and to
  consider the whole function $x$ as once and not pointwise,
  we write
  \begin{equation}\label{eq:basicproblem}
    \der x = F(x)
    \text{ (here) with } F \colon x ↦ (t ↦ f(x(t), t)).
  \end{equation}
  Additionally this generalization allows even more cases as we
  will see later in the applications.
  The main idea is to write this equivalently as
  \begin{equation*}
    x = \der^{-1} F(x)
  \end{equation*}
  and use the contraction mapping theorem. For this
  $\der^{-1} F$ must be a contraction on a suitable space.

  The biggest space that we have seen so far on which we were
  able to define differentiation is $\HiH{\w}0$ with
  $\der \colon \HiH{\w}0 ⟶ \HiH{\w}{-1}$.
  Hence we have to view $F$ as a function
  from $\HiH{\w}{0}$ to $\HiH{\w}{-1}$.

  Via the embedding of Lemma \ref{thm:EmbeddingHw0inHw-1} it is
  possible to extend $F$ if it is defined on smooth functions and
  maps to $\tstH \subset \HiH{\w}0$ but in many
  applications this is not suitable. One example is
  given in \autocite[Introduction and Section 5.1][5, 24]{TUD:HSDDE}.
  There an initial value problem is formulated in terms
  of linear functionals on a test space.

  We we have constructed the test space $\tstp+$ in Definition \ref{def:C+}
  such that integration is
  always possible and it lies in $\HiH{-\w}1$. Hence
  $F$ must be given as a mapping from $\tstH$ to $\dual{\tstp+}$
\end{textfold}
\subsection{Picard-Lindelőf}\label{sec:PicardLindeloef}
\begin{textfold}[Picard-Lindelőf motivation]
  In order to consider an element of $\dual{\tstp+}$ as an element of $\HiH{\w}{-1}$
  it must be continuous with respect to $\norm{·}_{-\w, 1}$.
  Hence we need the following condition on
  $F \colon \tstH ⟶ \dual{\tstp+}$:
  %
  For all $u ∈ \tstH$, there exists $K ∈ ℝ$, such that for all $ψ ∈ \tstp+$ we have:
  \begin{equation}\label{eq:FtoHiw-1}
    \abs{F(u)(ψ)} ≤ K \norm{ψ}_{-\w, 1}.
  \end{equation}
  %
  As noted above $\der^{-1} F$ must be a contraction in order
  to use the contraction mapping theorem. Since $\der^{-1}$ is unitary
  from $\HiH{\w}{-1}$ to $\HiH\w0$, $F$ must be a contraction
  (i.\,e.\ Lipschitz continuous with Lipschitz constant less than $1$) itself.
  Let the Lipschitz constant of $F$ be called $s$.
  Then we need the following condition for all $u, w ∈ \tstH$:
  \begin{equation*}
    \norm{F(u) - F(w)}_{\w, -1} ≤ s \norm{u - w}_{\w, 0}.
  \end{equation*}
  $F$ is not mapping to $\HiH{\w}{-1}$ by definition.
  So we write this without the $\HiH{\w}{-1}$-norm:
  (Also see \autocite[equation (4), 2nd part][13]{TUD:HSDDE}.)
  \begin{equation}\label{eq:FLipschitz}
    \abs{F(u)(ψ) - F(w)(ψ)} ≤ s \norm{ψ}_{-\w,1} \norm{u - w}_{\w, 0} \text{ for all } ψ ∈ \tstp+ .
  \end{equation}
  %
  Now we can simplify the first condition \eqref{eq:FtoHiw-1} to
  %
  \begin{equation}\label{eq:F(0)inHiw-1}
    \text{There exists $K ∈ ℝ$ such that for all $ψ ∈ \tstp+$: }
    \abs{F(0)(ψ)} ≤ K \norm{ψ}_{-\w, 1}
  \end{equation}
  %
  since \eqref{eq:F(0)inHiw-1} together with \eqref{eq:FLipschitz} implies for
  any $ϕ ∈ \tstH$ and any $ψ ∈ \tstp+$
  %
  \begin{align*}
    \abs{F(ϕ)(ψ)} &= \abs{(F(ϕ)(ψ) - F(0)(ψ)) + F(0)(ψ)} \\
                  &≤ \abs{F(ϕ)(ψ) - F(0)(ψ)} + \abs{F(0)(ψ)} \\
                  &≤ s \norm{ψ}_{-\w, 1} \norm{ϕ - 0}_{\w, 0} + K \norm{ψ}_{-\w, 1} \\
                  &≤ (s \norm{ϕ}_{\w, 0} + K) \norm{ψ}_{-\w, 1} ⇒ \eqref{eq:FtoHiw-1}
  \end{align*}
  %
  \begin{definition}[$F_{\w}$]\label{def:Fw}
    With \eqref{eq:FLipschitz} and \eqref{eq:F(0)inHiw-1} we can now extend
    $F$ to a Lipschitz continuous function $F_{\w}$ from $\HiH{\w}0$
    to $\HiH{\w}{-1}$ with Lipschitz constant $s < 1$.
  \end{definition} % end definition of $F_{\w}$
  %
  With this motivation we can formulate the first result that is well-known
  from the classical theory of differential equations: 
\end{textfold}
\begin{theorem}[Picard-Lindelőf, {\autocite[Theorem 3.2][13]{TUD:HSDDE}}] \label{thm:Picard-Lindelöf}
  Let $\w ∈ ℝ_{> 0}$, $s ∈ (0,1)$ and let
  $F \colon \tstH ⟶ \dual{\tstp+}$ such that the estimates \eqref{eq:FLipschitz} and
  \eqref{eq:F(0)inHiw-1} hold for $\w$. Then there exists a uniquely determined
  $u ∈ \HiH{\w}0$ such that
  \begin{equation*}
    \der u = F_{\w}(u) \text{ holds in } \HiH{\w}{-1}.
  \end{equation*}
\end{theorem} % end theorem Picard-Lindelőf
% proof not necessary, explained beforehand
\subsection{Higher regularity}
\label{sec:higher_regularity}
\begin{textfold}[Picard-Lindelőf higher regularity motivation]
  A slightly different approach can be taken starting from \eqref{eq:basicproblem}
  \begin{equation}
    \der u = F(u)
  \end{equation}
  by looking for a solution for $\der u$ instead of $u$ directly.
  Since $\der$ is unitary this is an
  equivalent problem.

  Call $v = \der u$. Then \eqref{eq:basicproblem} becomes
  \begin{equation*}
    v = F(\der^{-1} v).
  \end{equation*}
  In order to use the Theorem \ref{thm:Picard-Lindelöf} of Picard-Lindelőf write this as
  \begin{equation*}
    \der v = \der F(\der^{-1} v)
  \end{equation*}
  That means that $v ∈ \HiH{\w}{0}$, hence $F \colon \HiH{\w}1 ⟶ \HiH{\w}0$.
  Since $\der$ is unitary, $F$ must be a contraction.
  We stay with the assumption that $F$ is given as a function
  from $\tstH$ to $\dual{\tstp+}$. In order to view functionals
  on $\tstp+$ as elements of $\HiH{\w}0$ we identify
  $\HiH{\w}0$ with $\Hidual{(\HiH{-\w}0)}$ as in
  \ref{thm:EmbeddingHw0inHw-1}: $ϕ ∈ \HiH{\w}0$ is identified
  with $\HiH{-\w}0 \ni ψ ↦ ⟨ψ, ϕ⟩_{0,0}$.

  With this understanding we arrive at the conditions
  \begin{equation}\label{eq:FtoHiw0}
    \text{there is $K ∈ ℝ$, such that} \abs{F(0)(ψ)} ≤ K\norm{ψ}_{-\w, 0} \text{ for all } ψ ∈ \tstp+
  \end{equation}
  to ensure that the values of $F$ can be continuously extended to $\HiH{\w}0$.
  Secondly there exists $s ∈ (0, 1)$ such that for all $u, w ∈ \tstH$ and all $ψ ∈ \tstp+$
  \begin{equation}\label{eq:FLipschitztoHiw0}
    \abs{F(u)(ψ) - F(w)(ψ)} ≤ s \norm{ψ}_{-\w, 0} \norm{u - w}_{\w , 1}
  \end{equation}
  to ensure Lipschitz continuity.
\end{textfold}
\begin{corollary}[Picard-Lindelőf with higher regularity, {\autocite[Corollary 3.3][13]{TUD:HSDDE}}]
  \label{thm:Picard-LindelöfinHiw0}
  Let $\w ∈ ℝ_{> 0}$,
  $s ∈ (0,1)$ and let
  $F \colon \tstH ⟶ \dual{\tstp+}$ be such that
  there is
  $K ∈ ℝ_{> 0}$
  such that for all $u, w ∈ \tstH$ and
  $ψ ∈ \tstp+$ we have
  %
  \begin{equation*}
    \abs{F(0)(ψ)} ≤ K \norm{ψ}_{-\w, 0} \text{ and }
    \abs{F(u)(ψ) - F(w)(ψ)} ≤ s \norm{ψ}_{-\w, 0} \norm{u - w}_{\w, 1}.
  \end{equation*}
  %
  Denote by $F_{\w} \colon \HiH{\w}1 ⟶ \HiH{\w}0$ the
  strictly contractive extension of $F$. Then there is a unique
  $u ∈ \HiH{\w}1$ satisfying
  %
  \begin{equation*}
    \der u = F_{\w}(u) \text{ in } \HiH{\w}0.
  \end{equation*}
\end{corollary} % end theorem \autocite[Corollary 3.3][13]{TUD:HSDDE}
% again no proof because of explanation beforehand
\begin{textfold}[Gain: higher regualarity]  
  What have we gained with this corollary?
  The solution is not only existing and unique but also once (weakly)
  differentiable. In order to achieve this we need different conditions
  on the equation. $F$ must map into $\HiH{\w}0$, not only $\HiH{\w}{-1}$
  which is a stronger condition but on the other hand,
  in Corollary \ref{thm:Picard-LindelöfinHiw0} $F$ must be Lipschitz continuous
  with respect to the $\HiH{\w}1$ norm in contrast to
  the $\HiH{\w}0$ norm in Theorem \ref{thm:Picard-Lindelöf}.
  This is a weaker condition since
  \begin{equation}\label{eq:normcomparison}
    \norm{u}_{\w, 0}
    = \norm{\der^{-1} \der u}_{\w, 0}
    ≤ \frac 1{\w} \norm{u}_{\w, 1} \text{ for } u ∈ \HiH{\w}1
  \end{equation}
  whereas an estimate in the other direction is not possible because $\der$ is unbounded.
\end{textfold}
\begin{textfold}[why without contraction]  
  There is no reason to assume that $F$ is a contraction in differential equations of interest.
  With the equation \eqref{eq:normcomparison} this problem can be solved though if we
  choose $\w$ big enough. In the following theorem we cover the two cases at once but with
  slightly different arguments.
\end{textfold}
% Do not get confused by the k!
\begin{corollary}\label{thm:Picard-LindelöfNotcontraction}
  Let $k ∈ \{0, 1\}$, $C ∈ ℝ_{> 0}$, $\w > C$ and let $F \colon \tstH ⟶ \dual{\tstp+}$ be
  such that there exists $K ∈ ℝ_{> 0}$ such that for all $u, w ∈ \tstH$ and $ψ ∈ \tstp+$
  we have
  %
  \begin{equation*}
    \abs{F(0) (ψ)} ≤ K \norm{ψ}_{-\w, -k} \text{ and }
    \abs{F(u)(ψ) - F(w)(ψ)} ≤ C \norm{ψ}_{-\w, -k} \norm{u-w}_{\w, k}.
  \end{equation*}
  %
  We denote by $F_{\w} \colon \HiH{\w}k ⟶ \HiH{\w}k$ the unique continuous extension
  of $F$. Then there is a unique $u ∈ \HiH{\w}{k+1}$ with
  %
  \begin{equation*}
    \der u = F_{\w}(u) \text{ in } \HiH{\w}k.
  \end{equation*}
\end{corollary} % end corollary
\begin{proof}\imp{Case $k = 0$.}
  By \eqref{eq:normcomparison} the continuity requirement for $F$ implies for $u, w ∈ \tstH$, $ψ ∈ \tstp+$
  \begin{equation*}
    \abs{F(0) (ψ)} ≤ K \frac 1{\w} \norm{ψ}_{-\w, 1} \text{ and }
    \abs{F(u)(ψ) - F(w)(ψ)} ≤ \underbrace{C \frac 1{\w}}_{\define s < 1}
    \norm{ψ}_{-\w, 1} \norm{u-w}_{\w, 0}.
  \end{equation*}
  By Theorem \ref{thm:Picard-Lindelöf} $F$ has a contractive extension
  $\hat{F_{\w}} \colon \HiH{\w}0 ⟶ \HiH{\w}{-1}$ and a unique solution $u ∈ \HiH{\w}{0}$ such that
  \begin{equation*}
    \der u = \hat{F_{\w}}(u) \text{ in } \HiH{\w}{-1}.
  \end{equation*}
  Since $F_{\w}$ is the unique continuous extension of $F$ on $\HiH{\w}0$ this equation holds in
  $\HiH{\w}0$ which implies that $u ∈ \dom(\der) = \HiH{\w}1$.
% \end{proof}
% \begin{proof}
  
  \imp{Case $k = 1$.}
  In order to consider an element of $\dual{\tstp+}$ as an element of $\HiH{\w}1$,
  consider the canonical identification of a Hilbert space with its bidual:
  \begin{align*}
    \HiH{\w}1 &\isom \Hidual{(\Hidual{\HiH{\w}1})} = \Hidual{(\HiH{-\w}{-1})} \\
    \text{with the embedding } \dual{\tstp+} &⊂ \HiH{-\w}0 ⊂ \HiH{-\w}{-1}.
  \end{align*}
  The assumption guarantees that $F(u)$ can be extended to an element of $\HiH{\w}1$
  for all $u ∈ \tstH$.
  By \eqref{eq:norm0to-1} in Lemma \ref{thm:EmbeddingHw0inHw-1}
  the continuity requirement for $F$ implies for $u, w ∈ \tstH$, $ψ ∈ \tstp+$
  \begin{equation*}
    \abs{F(0) (ψ)} ≤ K \frac 1{\w} \norm{ψ}_{-\w, 0} \text{ and } \abs{F(u)(ψ) - F(w)(ψ)} ≤ \underbrace{C \frac 1{\w}}_{\define s < 1} \norm{ψ}_{-\w, 0} \norm{u-w}_{\w, 0}.
  \end{equation*}
  By Theorem \ref{thm:Picard-LindelöfinHiw0} $F$ has a contractive extension
  $\hat{F_{\w}} \colon \HiH{\w}1 ⟶ \HiH{\w}0$ and a unique solution
  $u ∈ \HiH{\w}1$ such that
  \begin{equation*}
    \der u = \hat{F_{\w}}(u) \text{ in } \HiH{\w}0.
  \end{equation*}
  Since $F_{\w}$ is the unique continuous extension of $F$ on $\HiH{\w}1$ this equation
  holds in $\HiH{\w}1$ which implies that $\der u ∈ \HiH{\w}1$, hence
  $u ∈ \dom\left(\der^2\right)$ which could be called $\HiH{\w}2$: $u$ is twice
  weakly differentiable.
\end{proof}
%
% Sascha says I should just not mention it: (2018-11-23)
% \subsection{Continuous dependence on conditions}
% \label{sec:continuousDependence}
% When treating differential equations one usually looks for well-posed problems
% since they usually arise in applications. For the existence and uniqueness of
% solutions the previous theorems give conditions on the problem.
% So far the continuous dependence on the \newTerm{data} $F$ is missing.
% In \autocite[Theorem 3.6, Remark 3.7][15\psq]{TUD:HSDDE} conditions
% on the data are given under which the solution is changing continuously.
% Here we will not go into this topic for two reasons.
% Firstly this section in the paper is not missing any intermediate
% steps and can therefore easily followed. Secondly the result
% appears to be quite weak due to the generality of the setting
% we are working in. For example even in the simple case of
% linear right hand sides we do not get any result.
\docEnd
