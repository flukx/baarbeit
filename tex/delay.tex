%! TEX program = lualatex

\input{.maindir/tex/header/preamble-section}
% inputs the preamble only if necessary

\docStart

\subsection{Delay} \label{sec:delay}
Causality is able to capture the notion of
\enquote{the result does not depend on the future}
but it does not distinguish between actually
depending on the past or not.
Intuitively one would not consider the
differential $x'$ in a typical ordinary
differential equation of the type
%
\begin{equation*}
  x'(t) = f(t, x(t))
\end{equation*}
%
to depend neither on the past
nor on the future of $x$. Hence we need
the analogue concept to causality for
\enquote{the result does not depend on the past}.
%
\begin{definition}[Amnesic, {\autocite[Definition~4.9][22]{TUD:HSDDE}}] \label{def:Amnesic}
  Let $X, Y$ be Hilbert spaces, $\w ∈ ℝ$. A mapping
  %
  \begin{equation*}
    W \colon \Hi{\w}0 ⊇ \dom(W) \tens X ⟶ \Hi{\w}0 \tens Y
  \end{equation*}
  %
  is called \newTerm{causal} if for all $a ∈ ℝ$, $x, y ∈ \dom(W)$
  \begin{equation*}
    \tro[>a](x - y) = 0 ⇒ \tro[>a](W(x) - W(y)) = 0:
  \end{equation*}
  if two arguments do not differ for arguments greater than $a$ then the
  images do not differ as well.
  This is equivalent to (for all $a ∈ ℝ$)
  \begin{equation*}
    \tro[>a] W = \tro[>a] W \tro[>a].
  \end{equation*}
  If a mapping $W$ is not amnesic, we say that
  $W$ \newTerm{has memory} or \newTerm{has delay}.
\end{definition} % end definition of Amnesic
%
In many ways causal and amnesic operators have
the same or \imp{dual} properties: there is
nothing special about $ℝ_{>a}$ in comparison
to $ℝ_{<a}$ but $\der^{-1}$ is causal
for $\w > 0$ but amnesic for $\w < 0$ as one
can see in \eqref{eq:integralposw} and
\eqref{eq:integralnegw}. Hence the same theory
as developed above
for amnesic operators would replace $\w > 0$
by $\w < 0$.

When we showed the causality of $F$ we actually
considered $\der^{-1} F$ because
$F$ maps into a space of functionals ($\dual{\tstp+}$ or $\Hi{\w}{-1}$)
and hence we needed $\der^{-1}$ to
get into a function space. This is justified
since $\der^{-1}$ is causal.

For defining amnesic or respectively having delay
for differential equations we have the same issue
and we cannot use $\der^{-1}$ since it is
not amnesic but we can use a duality similar to
the results in Section \ref{sec:dualities}. For the following
calculation note that taking inverses and adjoints
commute and due to unitarity $\exp(-\w \mo)^{-1} = \exp(-\w \mo)^*$
%
\begin{align*}
  \left(\der^{-1}\right)^*
  &= \left(\left(\exp(-\w \mo)^{-1} (\deriv + \w) \exp(-\w \mo) \right)^{-1}\right)^* \\
  &= \left( \exp(-\w \mo)^{-1} (\deriv + \w)^{-1} \exp(-\w \mo) \right)^* \\
  &= \exp(-\w \mo)^* ((\deriv + \w)^*)^{-1} \exp(-\w \mo) \\
  &= \exp(-\w \mo)^{-1} (-\deriv + \w)^{-1} \exp(-\w \mo) \\
  &= - \exp(-\w \mo)^{-1} \exp(\w \mo) \der[-\w]^{-1} \exp(\w \mo)^{-1} \exp(-\w \mo).
\end{align*}
%
By the Formula \eqref{eq:integralnegw}
in Corollary \ref{thm:ExplicitAntiderivativeFormula}
$\der[-\w]^{-1}$ is amnesic
and hence $\left(\der^{-1}\right)^*$ is
an amnesic operator from $\HiH{\w}{-1} ⟶ \HiH{\w}0$.
This allows us to \imp{define} what a differential equation
with delay \imp{is}.
%
\begin{definition}[Delay differential equation, {\autocite[Definition~4.11][22\psq]{TUD:HSDDE}}] \label{def:Delay_differential_equation}
  Let $\w > 0$ and $F ∈ \Con$. A differential equation of the form
  \eqref{eq:basicproblem}, i.\,e.,
  \begin{equation*}
    \der u = F(u)
  \end{equation*}
  is called a \newTerm{delay differential equation}
  if $\left(\der^{-1}\right)^* F$ has delay.
\end{definition} % end definition of Delay differential equation
% \TODO{show that some simple ode with discrete delay is a delay diff. eq.}
\docEnd
