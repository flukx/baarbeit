%! TEX program = lualatex

\input{.maindir/tex/header/preamble-section}
% inputs the preamble only if necessary

\docStart
%
\subsection{Operators on tensor products}
We study operators on Hilbert spaces. Hence we have to know how operators can be lifted to tensor
products (also see \autocite[Remark 2.9 (d)]{TUD:HSDDE}).
For pure tensors and hence for linear combinations it is obvious how to define it.
Following is
the proof that this is indeed well-defined and can be extended to the completion.
%
\begin{definition}[Algebraic Tensorproduct of linear operators]\label{def:AlgebraicTensorproductoflinearOperators}
  Let $H_1, H_2, K_1, K_2$ be complex Hilbert spaces and $A \colon H_1 ⊇ \dom(A) ⟶ K_1$, $B \colon H_2 \supset \dom(B) ⟶ K_2$ be linear operators. Then define the \newTerm{algebraic tensor product}
  \begin{align*}
    A \atens B \colon H_1 \tens H_2 \supset \dom(A) \atens \dom(B) &⟶ K_1 \tens K_2 \\
    x \tens y & ↦ Ax \tens By
  \end{align*}
  and extend it linearly.
  Then $A \atens B$ is a well-defined linear mapping.
  % Then $A \atens B$ is right-unique, that is, it is a mapping.
\end{definition} % end definition of Tensorproduct of linear operators
%
\begin{proof}
  For clarification note that here $A \atens B$ is considered as a subspace of $(H_1 \tens H_2) \times (K_1 \tens K_2)$ (i.\;e.\ the graph of $A \atens B$).

  $A \atens B$ is linear. Therefore it is enough to show that $(0, w) ∈ A \atens B$ implies $w = 0$.
  Let
  \begin{equation*}
    w = Σ_{i = 1}^n %a_i
    (A ϕ_i \tens B ψ_i) \text{ with } Σ_{i = 1}^n %a_i
    (ϕ_i \tens ψ_i) = 0
  \end{equation*}
  where $ϕ_i ∈ \dom(A)$, $ψ_i ∈ \dom(B)$ ($i = 1, …, n$).

  The sets $\setDef{ϕ_i}{i ∈ \{1,…,n\}}$
  and $\setDef{ψ_i}{i ∈ \{1, …, n\}}$ are not necessarily linear independent.
  Hence choose a linear independent set
  $\setDef{x_j ∈ \dom(A)}{j ∈ \{1, …, k\}}$ such that all $ϕ_i$,
  and a linear independent set
  $\setDef{y_l ∈ \dom(B)}{l ∈ \{1,…, m\}}$
  such that all $ψ_i$ can be represented as:
  \begin{equation*}
    ϕ_i = Σ_{j = 1}^k β_j^i x_j, \quad ψ_i = Σ_{l = 1}^m γ_l^i y_l \qquad i ∈ \{1, …, n\}.
  \end{equation*}
  Then
  \begin{align}
    \begin{split} \label{equ:ABsum}  
      w = Σ_{i = 1}^n %a_i
      (A ϕ_i \tens Bψ_i)
      &= Σ_{i = 1}^n %a_i \left (
      A \left ( Σ_{j = 1}^k β_j^i x_j \right ) \tens
      B \left ( Σ_{l = 1}^m γ_l^i y_l \right ) %\right )
      \\
      &= Σ_{i = 1}^n Σ_{j = 1}^k Σ_{l = 1}^m %a_i
      β_j^i γ_l^i \left ( A x_j \tens B y_l \right ) \\
    \end{split}
    \intertext{and}
    \begin{split}\label{equ:zeroSum}
      0 = Σ_{i = 1}^n %a_i
      (ϕ_i \tens ψ_i)
      = Σ_{i = 1}^n Σ_{j = 1}^k Σ_{l = 1}^m %a_i
      β_j^i γ_l^i \left ( x_j \tens y_l \right ).
    \end{split}
  \end{align}
  %
  For proving $w = 0$ we need to show that products of the form $%a_i
  β_j^i γ_l^i$ in \eqref{equ:ABsum}
  all vanish. By \eqref{equ:zeroSum} it
  suffices that
  $\setDef{x_j \tens y_l ∈ H_1 \tens H_2}{j ∈ \{1, …, k\}; l ∈ \{1, …, m\}}$
  is linear independent.

  Consider a linear combination
  $0 = Σ_{i = 1}^k Σ_{j = 1}^m α_{jl} (x_j \tens y_l)$ and
  $x ∈ H_1$, $y ∈ H_2$.
  Then
  \begin{align*}
    0 &= \left ( Σ_{j = 1}^{k} Σ_{l = 1}^m α_{jl} (x_j \tens y_l) \right ) (x, y)
    = Σ_{j, 1}^{k} Σ_{l = 1}^m α_{jl} ⟨x_j, x⟩_{H_1} ⟨y_l, y⟩_{H_2} \\
    &= ⟨ Σ_{j = 1}^k Σ_{l = 1}^m α_{jl} ⟨y_l, y⟩_{H_2} x_j , x ⟩.
  \end{align*}
  This holds for all $x ∈ H_1$. Thus we get
  $Σ_{i, j = 1}^{k, m} α_{ij} ⟨y_l, y⟩_{H_2} x_j = 0$.
  Since $\setDef{x_j}{j = 1, …, k}$ is linear independent, we get
  \begin{equation*}
    0 = Σ_{l = 1}^m α_{jl} ⟨y_l, y⟩ = ⟨Σ_{l = 1}^m α_{jl} y_l, y⟩ \text{ for } j = 1, …, k \text { and all } y ∈H_2.
  \end{equation*}
  Again we get $Σ_{l = 1}^m α_{jl} y_l = 0$ for $j ∈ \{1, …, k\}$
  and by linear independence $α_{jl} = 0$ for all
  $j ∈ \{1, …, k\}$ and $l ∈ \{1, …, m\}$.

  This concludes the proof.
\end{proof}
%
\begin{definition}[Tensorproduct of linear operators]\label{def:TensorproductiofLinearOperators}
  Let $A$, $B$ be linear operators and $A \atens B$ be closable.
  Then define $A \tens B$ as the closure of $A\atens B$.
\end{definition} % end definition of Tensorproduct of linear operators
%
As we will see in the following Lemma \ref{thm:TensoroperClosable} the existence proof
of the closure depends on the adjoint operators.
For the Definition \ref{thm:defadjoint} of adjoint operators
we needed that the operator is densely defined. For the algebraic tensor product of two
densely defined operators this is the case as we have to show.
%
\begin{lemma}[Dense subsets]\label{thm:DenseSubsets}
  Let $S_1 ⊆ H_1$ and $S_2 ⊆ H_2$ such that $\Lin S_i$ is dense in $H_i$
  ($i ∈ \{1,2\}$).
  Then $S_1 \atens S_2$ is dense in $H_1 \tens H_2$.
\end{lemma} % end lemma Dense subsets
\begin{proof}
  Firstly consider a pure tensor $x \tens y ∈ H_1 \tens H_2$ that is to be
  approximated by elements of $S_1 \atens S_2$.

  There exist sequences
  $(x_n)_{n ∈ ℕ}$ in $\Lin S_1$ and $(y_n)_{y ∈ ℕ}$ in $\Lin S_2$, such that
  $x_n ⟶ x$ in $H_1$ and $y_n ⟶ y$ in $H_2$ as $n ⟶ ∞$.
  Then $x_n \tens y_n ∈ \Lin S_1 \atens \Lin S_2 = S_1 \atens S_2$.
  % It is not surprising that $x_n \tens y_n ∈ S_1 \atens S_2$:
  % Let $k, m ∈ ℕ$, $s^1_1, …, s^1_k ∈ S_1$, $s^2_1, …, s^2_m ∈ S_2$,
  % $α_{1,1}, …, α_{1,k}, α_{2,1}, …, α_{2, m} ∈ ℂ$ such that
  % \begin{equation*}
  %   x_n = Σ_{i = 1}^k α_{1,i} s^1_i, \quad
  %   y_n = Σ_{j = 1}^m α_{2,j} s^2_j.
  % \end{equation*}
  % Then
  % \begin{equation*}
  %   x_n \tens y_n = \left( Σ_{i= 1}^k α_{1,i} s^1_i \right)
  %   \tens \left( Σ_{i= 1}^m α_{2,i} s^2_i \right)
  %   = Σ_{i= 1}^k Σ_{j = 1}^m α_{1,i}α_{2,j} (s^1_i \tens s^2_j) ∈ S_0 \atens S_1.
  % \end{equation*}
  For the approximation of $x \tens y$ by $x_n \tens y_n$ reduce it to
  approximation in $H_1$ and $H_2$:
  \begin{align*}
    \norm{x \tens y - x_n \tens y_n}_{H_1 \tens H_2}
    &≤ \norm{x_n \tens y_n - x \tens y_n} + \norm{x \tens y_n - x \tens y}_{H_1 \tens H_2}
    \quad \text{Triangle ineq.} \\
    &= \norm{(x_n - x) \tens y_n}_{H_1 \tens H_2} + \norm {x \tens (y_n - y)}_{H_1 \tens H_2} \\
    \overset{\eqref{equ:tensorsymmetry}}&= \norm{x_n - x}_{H_1} \norm{y_n}_{H_2} +
    \norm x_{H_1} \norm {y_n - y}_{H_2} \\
    &→ 0 · \norm y_{H_2} + \norm x_{H_1} · 0 → 0 \quad (n ⟶ ∞)
  \end{align*}
  % since $\norm {y_n}_{H_2}$ is bounded by,
  % let us say, $2 \norm y_{H_2}$ for sufficiently large $n$.
  Linear combinations of pure tensors (that is, elements of $H_1 \atens H_2$),
  can be approximated by approximating the summands. Since $H_1 \atens H_2$ is dense
  in $H_1 \tens H_2$ by Definition \ref{def:tensorproduct},
  $S_1 \atens S_2$ is dense in $H_1 \tens H_2$.
\end{proof}
%
\begin{lemma}\label{thm:TensoroperClosable}
  Let $A \colon H_1 ⊇ \dom(A) ⟶ K_1$, $B \colon H_2 ⊇ \dom(B) ⟶ K_2$ be densely defined, closable, linear operators.
  Then $A \atens B$ is closable and
  \begin{align} \label{equ:AtensBClosable}
    A \tens B = \cl{A \atens B} ⊆ (A^* \atens B^*)^*
  \end{align}
\end{lemma} % end lemma TensoroperClosable
%
\begin{proof}
  It is enough to show \ref{equ:AtensBClosable} since a closed subset of the (graph of the) closed operator
  $(A^* \atens B^*)^*$ must be a closed operator.
  $A^*$ and $B^*$ are densely defined by Lemma \ref{thm:closability} since $A$ and $B$ are closable.
  Hence $A^* \atens B^*$ is densely defined by Lemma \ref{thm:DenseSubsets} and thus $(A^* \atens B^*)^*$
  is a well-defined, closed, linear operator by Lemma \ref{thm:adjointClosed}.
  Since $(A^* \atens B^* )^*$ is closed, it is enough to check, that every element $η$ of
  $\dom(A \atens B)$ satisfies
  \begin{equation*}
    % ⟨ (A \atens B) η, ξ ⟩_{K_1 \tens K_2} = ⟨ η, (A^* \atens B^*) ξ⟩_{H_1 \tens H_2}
    ⟨ (A^* \atens B^*) ξ, η⟩_{H_1 \tens H_2} = ⟨ξ, (A \atens B) η ⟩_{K_1 \tens K_2}
    \text{ for all $ξ ∈ \dom(A^* \atens B^*)$}
  \end{equation*}
  since this implies $η ∈ \dom((A^* \atens B^*)^*)$
  and $(A^* \atens B^*)^* η = (A \atens B) η$.
  Write
  \begin{align*}
    η &= Σ_{j = 1}^m u_j \tens v_j \quad \text{($m ∈ ℕ$, $u_j ∈ \dom(A)$, $v_j ∈ \dom(B)$, $j ∈ \{1, …, m\}$)} \\
    \text {and } ξ &= Σ_{i = 1}^n x_i \tens y_i \quad \text{($n ∈ ℕ$, $x_i ∈ \dom(A^*)$, $y_i ∈ \dom(B^*)$, $i ∈ \{1, …, n\}$)}.
  \end{align*}
  Then
  \begin{align*}
    ⟨ (A^* \atens B^*) ξ, η⟩
    &= Σ_{i = 1}^n Σ_{j = 1}^m ⟨A^* x_i, u_j⟩_{K_1} ⟨B^* y_i, v_j⟩_{K_2} \\
    &= Σ_{i = 1}^n Σ_{j = 1}^m ⟨x_i, A u_j⟩_{K_1} ⟨y_i, Bv_j⟩_{K_2} \\
    &= ⟨ ξ, (A \atens B) η ⟩_{K_1 \tens K_2}. \qedhere
  \end{align*}
\end{proof}
%
\begin{example} \label{ex:AtensId}
  The only case of operators on tensor products we will see are those of
  the form $A \tens \id \colon H_1 \tens H_2 \supset \dom(A) \tens H_2$
  with $\id$ being the identity on $H_2$.
  In those cases we will write $A$ both to denote
  $A \colon H_1 \supset \dom(A)$ as well as $A \tens \id$.
  In the case of bounded $A$ we
  have for the operator norm
  $\opnorm{A}{H_1} = \opnorm{A \tens \id}{H_1 \tens H_2}$ as one can easily
  check for pure tensors.
\end{example}

\docEnd
