%! TEX program = lualatex
\input{.maindir/tex/header/preamble-section}

\docStart
\section{Introduction} \label{sec:introduction}
\subsection{Motivation} \label{sec:motivation}
% \TODO{describe how Hilbert spaces do solution theory}
% done by talking about contraction idea
An analysis course for undergraduate students about ordinary differential
equations (ODE) usually includes some solution techniques
for special types of equations that
can be solved explicitly together with
the existence theorem of Peano and the existence
and uniqueness theorem of Picard-Lindelőf.
Both give local results and differentiable solutions.
The right-hand side needs to be at least continuous.

One direction of research consists of \newTerm{delay differential equations} (DDE).
A DDE differs from an ODE by allowing a dependence of the right-hand
side on the past of the unknown.
One example from infection studies given in 
\autocite[Introduction][3]{Hale:FctDiffEq} is
\begin{equation*}
  \dot I (t) = λ (I(t - L_1) - I(t - L_2))
  \quad \text{for }t ∈ ℝ, I(t) ∈ ℝ_{> 0}, L_1, L_2 ∈ ℝ_{< 0}, λ ∈ ℝ_{> 0}.
\end{equation*}
This is a DDE with discrete delay as studied in Section
\ref{sec:discreteDelay}.
%which models the number of infected people suffering from gonorrhea.
This is not covered by
the theorem of Picard-Lindelőf from usual undergraduate courses.
There exist a big variety of solution techniques and solution property
analysis, most of them limited
to some classes of DDEs.
For example \autocite{Gil:IntOpDDE} examines periodicity for equations
with discrete delay
% of the form
% \begin{equation*}
%   \dot x (t) = f(x(t), x(t - h))
% \end{equation*}
with the background of mechanical applications and considers
operators on function spaces of continuous function.
Common topics are also stability and bifurcation, as in
\autocite{Gop:Stab}.
A list of examples as well as a historical overview can be
found in \autocite{Hale:FctDiffEq} which studies different
types of DDEs like linear
systems and equations of neutral type with a long
list of tools.
%examples:
% https://projecteuclid.org/download/pdf_1/euclid.jiea/1181075393
% from https://projecteuclid.org/euclid.jiea/1181075393
% works with continuous functions
% limits to ∂x(t) = f(x(t), x(t - h)), looks at periodicity
% has example from mechanics at beginning

% cites a lot of examples, gives a historical overview
% starts at simple equations, includes study of limit behavior
% often explicit solution finding
% neutral differential equations (take into account derivative of past)

% https://link.springer.com/book/10.1007%2F978-1-4612-4206-2
% textbook
% uses several different techniques like functional analysis, semigroups, dynamical systems; uses DDEs as good example to learn those topics
% looks at examples

For partial differential equations (PDEs), the approach of Picard-Lindelőf
and explicit solution formulas do not suffice in most cases.
Functional analysis and in particular Hilbert space theory on the other
hand is a great tool to
show the existence and uniqueness of solutions to many types of PDEs.
Usually one gives up the requirement of differentiability of
the solution and replace it with the notion of weak differentiability.
A standard textbook on this is \autocite{Evans:PDE}.

The approach of \autocite{TUD:HSDDE} and my thesis uses suitable Hilbert spaces
to formulate a solution theory for a wide class of ordinary differential
equations. This includes the employment of weak differentiability.
Then several types of delay differential equations are
relatively simple special cases and the main motivation for the Hilbert space approach
of the paper \autocite{TUD:HSDDE}.

Applying the functional analysis notions of Hilbert space adjoints and
weak differentiability to ordinary and delay differential equations
might appear to be unnecessary complicated.
Nevertheless, there are several reasons why it might be a reasonable
alternative that can be taught in a seminar:
% Furthermore
% the proofs are technical in the sense that the treatment of the correct
% definition ranges of the solution obfuscate the central idea of the proof
% which is -- for Picard-Lindelőf -- a contraction argument.
%
\begin{enumerate}
  \item As just noted, being firm in Hilbert space theory
    is a strong background for PDEs afterwards.
    Especially, the concept of weak differentiability becomes familiar.
  \item Once we can state Picard-Lindelőf using Hilbert space operators,
    the central contraction idea can stand out in its simplicity.
    In the classical proof of Picard-Lindelőf on the other hand, one has to take
    care of the domain of the involved Picard-iterates and restrict it to
    a small neighborhood of the initial time. The used space of
    continuous functions with the supremum norm is a Banach space.
    The $\LpD$-type spaces are Hilbert spaces, which enlarges the
    available toolbox. %more complicated to handle
    %than $\Lp$-type Hilbert spaces.
  \item The classical versions of Peano and Picard-Lindelőf only consider
    the case of differentiable solution and continuous right-hand sides.
    A wide range of applications is not covered by this though.
    Some examples are given in the introduction of \autocite{Hajek:Discont}.
    % \TODO{references, maybe from \autocite{TUD:HSDDE}?}
    % One big class of ordinary differential equations
    % that are studied are delay equations. Adjusting Picard-Lindelőf to
    % those cases is not trivial while in the setting regarded in \autocite[][]{TUD:HSDDE}
    % those are just a special case and were the main motivation for the paper in the first place.
  \item As mentioned above, classical solution theory is usually only local.
    % and global solution theory is based on the local results.
    In particular, it is necessary to restrict the solution to a small
    interval in order to make the Picard-iteration a contraction.
    Here, on the other hand, we will regard global solutions.
    That means, that we always regard functions that are defined on
    the entire real line.
    Instead of restricting the solution
    we choose the solution space appropriately.
    %enables us to turn every
    %Lipschitz continuous right-hand side into a contraction.

    In \autocite[Section 5.2][28 ff.]{TUD:HSDDE} an
    application to local problems is given, but this will be beyond the scope
    of this paper.
\end{enumerate}
With global in time solutions comes the notion of causality which
captures the idea that a solution up to any time cannot depend on the future
but only on the past and present.
Hence, necessary properties of the differential equation
that ensure causal solution operators, are presented.
For a wider view on causality, see \autocite{Lak:Causal}
which focusses solely on different types of causal
differential equations.
% https://www.worldscientific.com/worldscibooks/10.1142/l012#t=aboutBook
% talks about different types of causal differential equations
% collects different works

% \subsection{Stylistic approach
\subsection{Personal motivation}
My aim is to give the reader a smooth guide to the outlined solution theory.
This thesis is mostly self-contained, presenting just the parts and cases of
Hilbert space operator theory needed.

My goal is to present not only %correct
statements
and proofs but also ideas on why the regarded theory is developed and how
one can go about proving the statements. I filled the gaps I found myself stuck in
during studying the paper \autocite{TUD:HSDDE}.
I hope that in this way
others can easily follow the presented thoughts.
To support this connection to \autocite{TUD:HSDDE}, references
to the respective sections are given throughout.

In order to limit the extend of this thesis, some topics of
\autocite{TUD:HSDDE} are left out. They are not essential to the central
solution theory.

I am grateful for Professor S.\ Siegmunds invitation to study
this topic and the continued motivation as well as for the very supporting
supervision by Dr.\ S.\ Trostorff. I also want to thank Niklas Jakob and
Max Bender for their very detailed and constructive criticism.

% , namely Fourier transformation (\autocite[Section 2][7\psq]{TUD:HSDDE}),
% functional calculus (\autocite[Definition 2.10 and following][11\psq]{TUD:HSDDE})
% and its applications (\autocite[Section 5.3.2][34\psq]{TUD:HSDDE}),
% the continuous dependence on the data.
% (\autocite[Theorem 3.6][15\psq]{TUD:HSDDE}), initial value problems
% (\autocite[Section 5.1][24\psqq]{TUD:HSDDE}) and local solvability
% (\autocite[Section 5.2][28\psqq]{TUD:HSDDE}).
%
\subsection{Structure} \label{sec:structure}
Based on the work of the previous chapters, the solution theory is presented
in Section \ref{sec:basic_solution_theory}.
At first, the existence and uniqueness of solutions is discussed.
Then in Section \ref{sec:causality} the concept of causality is
introduced.% already in Goal section: that describes that the
% solution of a differential equation only depends on the equation
% in the past.

A solution theory for (differential)
equations has to analyse conditions on the equation that can guarantee
the existence and uniqueness of a solution in a set of solution candidates.
The candidates are a variant of $\LpD$-spaces with an exponential weight
and are introduced in Section~\ref{sec:extrapolation_spaces}.
In order to allow distributions as right-hand sides, we embed
the weighted $\LpD$-spaces into so called extrapolation spaces.
They are introduced in Section~\ref{sec:gelfand_triple}.
On the other hand, this also allows us to get a glimpse on
regularity theory in Section~\ref{sec:higher_regularity}.

Since it is no more work to consider Hilbert space valued
functions than $ℂ$ valued functions, we consider
Hilbert space valued functions throughout.
The background is
presented in Section~\ref{sec:tensorproduct}.

Since $\LpD$
includes non-differentiable functions, the question arises what differentiation
as an operator on $\LpD$ means.
For this purpose we introduce differentiation as a closable operator
on the Hilbert space $\Lp{ℝ}$.
Hence the needed aspects on closures
of (unbounded) linear operators
are introduced in Section \ref{sec:Close}.
Since differentiation is firstly defined on test functions, the
density of test functions in $\Lp{ℝ}$ needs to be shown, see
Section \ref{sec:TestfctsDenseInL2}.
The extension to weighted $\LpD$ spaces is done in Section \ref{sec:weightedspace}
while the further extension to extrapolation spaces
is done in Theorem \ref{thm:derivative_unitary}.

After the general theory, some special cases that
can be successfully tackled by the developed solution theory are inspected.
This includes
ordinary differential equations (Section \ref{sec:nemitzki}),
as well as
delay differential equations with discrete (Section \ref{sec:discreteDelay}) and
continuous delay (Section \ref{sec:continuousDelay}).

\textinput{notation}
\docEnd
