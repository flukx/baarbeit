%! TEX program = lualatex
\input{.maindir/tex/header/preamble-section}

\docStart

\section{Gelfand triple}
\label{sec:gelfand_triple}
\begin{textfold}[Explain Hiw1]
  So far we have defined the differentiation on a sub (vector) space
  $\dom(\der)$ of $\HiH{\w}0$ and as $\der$ is unbounded
  there is no way to extend it continuously onto all of $\HiH{\w}0$
  mapping to $\HiH{\w}0$. In order to apply methods from functional
  analysis we rather want to consider an operator on a Hilbert space,
  not on a non-complete pre-Hilbert space.
  So we make $\dom(\der)$ a Hilbert space in the following way:
\end{textfold}
\begin{definition}[$\Hi{\w}1$, {\autocite[Def. 2.6][9]{TUD:HSDDE}}] \label{def:Hw1}
  For $\w ∈ ℝ\without \{0\}$ define $\Hi{\w}1$ as
  $(\dom(\der), ⟨·, ·⟩_{\w, 1})$ with the inner product
  $⟨f, g⟩_{\w, 1} = ⟨\der f, \der g⟩_{\w, 0}$ for $f, g ∈ \Hi{\w}1$.
  It follows for the norm:
  $\norm f_{\w, 1} = \norm {\der f}_{\w, 0}$.
  % $\HiH{\w}1$ is $\dom(\der)$ as a mapping on $\HiH{\w}0$. % Keinen blassen Schimmer, was das mal bedeuten sollte.
  \begin{note}\label{note:H1notsubspace}  
    Note that even though $\dom(\der)$ is as a vector space a subspace of $\HiH{\w}0$,
    $\HiH{\w}1$ is \imp{not} a sub Hilbert space because
    $\norm{·}_{\w, 1} ≠ \norm{·}_{\w, 0}$.
  \end{note} % end note
\end{definition} % end definition of $\Hi{\w}1$
\begin{textfold}[Explain Hiw-1]
  Equipped with $\HiH{\w}1$ we could write our equation
  to hold in $\HiH{\w}0$ with the solution being an element of
  $\HiH{\w}1$ but there are common use cases where the right
  hand side of a differential equation cannot be formulated
  as a function but as a linear functional on functions.
  The most prominent example are initial value problems as
  described in \autocite[Section 5.1][24]{TUD:HSDDE}. We
  model this by the following space.
\end{textfold}
\begin{definition}[Extrapolation space $\Hi{\w}{-1}$]\label{def:Hw-1}
  For $\w ∈ ℝ\without \{0\}$ define $\Hi{\w}{-1} \definedas \Hidual{\Hi{-\w}1}$
  as the space of all continuous
  linear functionals on $\Hi{-\w}1$ with the usual operator norm, denoted by $\norm{·}_{\w,-1}$.
  Pay attention to the sign of $\w$!
  $Hi{\w}{-1}$ can be called \newTerm{extrapolation space}.

  $\HiH{\w}{-1} \isom \Hidual{\left(\HiH{-\w}1\right)}$ as expected.
\end{definition} % end definition of $\Hi{\w}{-1}$
\begin{note}[Degree of differentiability]\label{note:DegreeOfDifferentiability}
  The index $-1$, $0$ and $1$ indicate the degree of differentiability.
  Functions in $\HiH{\w}1$ can be differentiated once in the sense of $\der$.
  Functions in $\HiH{\w}0$ in general cannot be derived but we will see
  in Theorem \ref{thm:derivative_unitary}
  that we can make sense of $\der$ as an operator from $\HiH{\w}0$ to
  $\HiH{\w}{-1}$, so \enquote{first antiderivatives} of elements of $\HiH{\w}{-1}$
  are not differentiable but second antiderivatives are,
  justifying the intuition of a differentiability
  degree of $-1$.
\end{note} % end note Degree of differentiability
% It is common to identify a Hilbert space with its dual by the Riesz representation theorem.
%
\begin{textfold}[motivation Hi0->-1 embedding]  
  Since $\der^{-1}$ is continuous as a mapping from $\HiH{\w}0$ to $\HiH{\w}0$,
  the identity map is a continuous embedding of $\HiH{\w}1$ into $\HiH{\w}0$.
  We want to have the same for $\HiH{\w}0$ into $\HiH{\w}{-1}$ and get it
  by a typical identification of functions with functionals: integration.
  We want to identify a function $u$ with the functional
  \begin{equation*}
    ψ ↦ ∫_{ℝ}⟨u, ψ⟩_H = ⟨u, ψ⟩_{0, 0}.
  \end{equation*}
  Since neither elements of
  $\HiH{\w}0$ nor elements of $\HiH{\w}1$ are square-integrable one needs $\exp(-\w \mo)$.
  That would yield for $f ∈ \HiH{\w}0$, $g ∈ \HiH{\w}1$:
  \begin{align*}
    \underbrace{⟨f, g⟩_{0,0}}_{\text{not well-defined}}
    = \underbrace{∫_{ℝ}⟨f, g⟩_H}_{\text{We want that}}
    \neq \underbrace{⟨f, g⟩_{\w, 0}}_{\text{defined}}
  \end{align*}
  The trick chosen here is to not use $g ∈ \HiH{\w}1$ but $g ∈ \HiH{-\w}1$. This way the $\exp$ cancel
  and the inner product in $\Lp{ℝ} \tens H$ is well-defined while calculated in the naive way. This is
  the reason, why $\HiH{\w}{-1}$ is defined as $\Hidual{\left(\HiH{-\w}1\right)}$
  and not $\Hidual{\left(\HiH{\w}1\right)}$.
  \begin{align*}
    \underbrace{⟨f, g⟩_{0,0}}_{\text{not well-defined}}
    = \underbrace{∫_{ℝ}⟨f, g⟩_H}_{\text{We want that}}
    = \underbrace{∫_{ℝ}⟨\overbrace{\exp(-\w \mo)f}^{∈ \Lp{ℝ} \tens H},
    \overbrace{\exp(\w \mo) g}^{∈ \Lp{ℝ}\tens H}⟩}_{\text{We do have this}}
    % = ⟨f, g⟩_{\w, 0}  % that's plain wrong
    \define \underbrace{⟨f, g⟩_{0,0}}_{\text{Now defined!}}
  \end{align*}
\end{textfold}
\begin{lemma}[Embedding, {\autocite[Remark~2.7][9]{TUD:HSDDE}}] \label{thm:EmbeddingHw0inHw-1}
  A function $ϕ$ in $\HiH{\w}0$ is identified with the following linear continuous functional in
  $\HiH{\w}{-1}$:
  \begin{equation}\label{eq:EmbeddingHw0inHw-1}
    ψ ↦ ⟨\exp(-\w m)ϕ, \exp(\w m) ψ⟩_{0,0} \define ⟨ϕ, ψ⟩_{0,0}
  \end{equation}
  This embedding is conjugate-linear and continuous with
  %
  \begin{equation}\label{eq:norm0to-1}
    \norm{ψ}_{\w, 1} ≤ \frac 1{\w} \norm{ψ}_{\w, 0}.
  \end{equation}
  In the same way we identify $\HiH{\w}0$ with
  $\Hidual{(\HiH{-\w}0)}$. By the Fréchet-Riesz representation theorem
  (see \autocite[Theorem V.3.6][228]{DWerner:FuAna})
  and since $\exp(\pm \w \mo)$ is isometric
  this identification is isometric.
  %
  % and its image is dense in $\Hi{\w}{-1}$.
  % shown at end of section
\end{lemma} % end note Embedding
\begin{proof}
  Call the embedding $ι$.
  It is to be checked that for $f ∈ \HiH{\w}0$ the functional $ι(f)$ is indeed linear,
  continuous and the $ι$ is conjugate-linear and
  continuous.
  Let $ϕ ∈ \HiH{\w}0$, $ψ ∈ \HiH{-\w}1$.
  The linearity of $ι(ϕ)$ and the conjugate-linearity of $ι$ are clear
  since $\exp(\pm \w \mo)$ is linear and $⟨·,·⟩$ is conjugate bilinear.
  Then we have to find a $C ∈ ℝ$ independent of
  $ψ$ ($⇒$ the functional is continuous) and
  $ϕ$ ($⇒$ the embedding is continuous) such that
  \begin{equation*}
    \abs{⟨\exp(-\w \mo) ϕ, \exp(\w \mo)ψ⟩_{0,0}} ≤ C \norm{ψ}_{-\w,1} \norm{ϕ}_{\w,0}
  \end{equation*}
  The typical tool for estimation of inner products is the Cauchy-Schwarz inequality.
  The other ingredient is the fact that the identity embedding from $\HiH{-\w}1$ into
  $\HiH{-\w}0$ is continuous because $\der[-\w]^{-1}$ is continuous:
  \begin{align*}
    \abs{⟨\exp(-\w \mo) ϕ, \exp(\w \mo)ψ⟩_{0,0}}
    &≤ \norm{\exp(-\w \mo)ϕ}_{0,0} \norm{\exp(\w \mo)ψ}_{0,0} \\
    &= \norm{ϕ}_{\w, 0} \norm{ψ}_{-\w, 0} \\
    &= \norm{ϕ}_{\w, 0} \norm{\der[-\w]^{-1} \der[-\w] ψ}_{-\w, 0} \\
    &\qquad \text{(well-defined since $ψ∈\dom(\der[-\w])$)} \\
    &≤ \norm{ϕ}_{\w, 0} \opnorm{\der[-\w]^{-1}}{\Hi{-\w}0} \norm{\der[-\w] ψ}_{-\w, 0} \\
    \overset{\text{Def. \ref{def:Hw1}}}&=
    \norm{ϕ}_{\w, 0} \opnorm{\der[-\w]^{-1}}{\Hi{-\w}0} \norm{ψ}_{-\w, 1} \\
    % &\qquad \text{(Def. \ref{def:Hw1} $\norm{·}_{-\w,1}$)} \\
    \overset{\ref{thm:diffContinuouslyInvertible}}&=
    \norm{ϕ}_{\w, 0} \frac 1{\abs{-\w}} \norm{ψ}_{-\w, 1} \\
    % &\qquad \text{\autocite[Cor. 2.5 (c)][8]{TUD:HSDDE}} \\
    ⇒ \norm{ϕ}_{\w, -1} &≤ \frac 1{\abs{\w}} \norm{ϕ}_{\w, 0}
  \end{align*}
  With $C = \frac1{\abs{\w}} = \frac1{\abs{-\w}}$ we are done.

  % Secondly it is to be shown that
  % $\Hi{\w}0$ is dense in $\Hi{\w}{-1}$. This will be done by showing
  % that $\Hi{\w}0$ is as a subspace of $\Hi{\w}{-1}$ is isomorphic to
  % $\dom(\der)$ as a subspace of $\Hi{\w}0$.
  %
  % Let $Ψ ∈ \Hi{\w}{-1}$. Then there exists a $ψ ∈ \Hi{-\w}1$ such that
  % for all $φ ∈ \Hi{-\w}1$ we have
  % \begin{align*}
  %   Ψ(φ) &= ⟨φ, ψ⟩_{-\w, 1} = ⟨\der[-\w] φ, \der[-\w] ψ⟩_{-\w, 0} = ⟨φ, \der[-\w]^* \der[-\w] ψ⟩_{-\w, 0}.
  %   \intertext{As we will see later in \eqref{eq:adjoint_d} this can be written as}
  %   &= ⟨φ, \exp(\w \mo)^{-1} \exp(-\w \mo) (-\der) \exp(-\w \mo)^{-1} \exp(\w \mo)
  %   \der[-\w] ψ⟩_{-\w, 0} \\
  %   &= ι((-\der) \exp(-\w \mo)^{-1} \exp(\w \mo) \der[-\w] ψ)(φ)
  % \end{align*}
  % As we can see, $Ψ$ is in $ι(\Hi{\w}0)$ if and only if,
  % $\exp(-\w \mo)^{-1} \exp(\w \mo) \der[-\w] ψ$ is in $\dom(\der)$.
  % Since the following are isometries:
  % \begin{align*}
  %   \Hi{\w}{-1} = \Hi{-\w} 1^* \stackrel {Ψ ↦ ψ}{\isom} \Hi{-\w}1
  %   \stackrel{\der[-\w]}{\isom} \Hi{-\w}0
  %   \stackrel{\exp(\w \mo)}{\isom} \Hi00
  %   \stackrel{\exp(-\w \mo)^{-1}}{\isom} \Hi{\w}0
  % \end{align*}
  % and $\dom(\der)$ is dense in $\Hi{\w}0$, $ι(\Hi{\w}0)$ is dense in $\Hi{\w}{-1}$.
\end{proof}
%
At this point it also becomes evident why we have to choose $\w \neq 0$.
Otherwise the identity is not
a continuous embedding from $\Hi{-\w}1$ into $\Hi{-\w}0$
and this construction does not work.
%
\begin{note}[Gelfand triple, {\autocite[Theorem 2.8][9]{TUD:HSDDE}}] \label{note:GelfandTriple}
  The triple 
  \begin{equation*}
    (\HiH{-\w}1, \HiH00, \HiH{\w}{-1})
  \end{equation*}
  with $\HiH00 = \Lp{ℝ} \tens H$ identified with $\HiH{-\w}0$ and
  $\HiH{\w}0$ via $\exp(\pm \w \mo)$ is called a \newTerm{Gelfand triple}.
  \begin{definition}[Rigged Hilbert space] \label{def:rigged_hilbert_space}
    In general a Gelfand triple or \newTerm{rigged Hilbert space}
    is a triple $(K_0, K_1, \Hidual{K_0})$ with $K_1$ being a Hilbert space,
    $K_0 \hookrightarrow K_1$ a densely embedded vector space
    with a finer topology such that
    the inclusion map $ι \colon K_0 → K_1$ is continuous.
    Then $ι^*$ maps from $\Hidual{K_0}$ to
    $\Hidual{K_1} \isom K_1$, is continuous as well with
    $ι^*φ(ψ) = ⟨φ, ψ⟩_{K_1}$ for
    $φ ∈ K_0$, $ψ ∈ K_1$ and embeds $K_1$ densely into $\Hidual{K_0}$.
  \end{definition} % end definition of Rigged Hilbert space
  \begin{proof}
    For simplicity assume that $K_0$ is a Hilbert space as well.
    In the case we are interested in this is the case and the
    proof for the general case needs no additional idea.

    First consider the operator norm of $ι^*$. Let $φ ∈ K_1$,
    $ψ ∈ K_0$ with $\norm ψ_{K_0} = 1$. Then
    \begin{align*}
      \abs{(ι^* φ)(ψ)} = \abs{⟨φ, ιψ⟩_{K_1}}
      & \leq \norm{φ}_{K_1} \norm{ι}_{L(K_0, K_1)} \norm{ψ}_{K_0} \\
      ⇒ \norm{ι^* φ}_{\Hidual{K_0}} &\leq \norm{φ}_{K_1} \norm{ι}_{L(K_0, K_1)} \\
      ⇒ \norm{ι^*}_{L(K_1, \Hidual{K_0})} &\leq \norm{ι}_{L(K_0, K_1)}.
    \end{align*}
    So $ι^*$ is continuous. $ι^*$ is injective since $ι(K_0)$ is dense
    in $K_1$ and $⟨φ_1, ψ⟩_{K_1} = ⟨φ_2, ψ⟩_{K_1}$ for $φ_1, φ_2 ∈ K_1$
    and all $ψ$ in a dense subset of $K_1$ implies $φ_1 = φ_2$.

    We want to show that $ι^*$ has dense range, that is
    $\left(ι^*\right)^{-1}$ has dense domain. First show that taking the adjoint
    and the inverse commute. Note that $\left(ι^{-1}\right)^*$ is well-defined
    since $ι^{-1}$ is densely defined.
    Then we have the equivalences
    \begin{align*}
      Φ ∈ \dom\left(\left(ι^*\right)^{-1}\right)
      &⇔ ∃ φ ∈ K_1 ∀ ψ ∈ K_0: Φ(ψ) = ⟨φ, ιψ⟩_{K_1} \\
      &⇔ ∃ φ ∈ K_1 ∀ ς ∈ ι(K_0): Φ\left(ι^{-1}ς\right) = ⟨φ, ς⟩_{K_1} \\
      &⇔ \left(ς ↦ Φ\left(ι^{-1}ς\right)\right) \text{ is continuous on } ι\left(K_0\right)
      ⇔ Φ ∈ \dom\left(\left(ι^{-1}\right)^*\right).
    \end{align*}
    Here we see that
    $\dom\left(\left(ι^{-1}\right)^*\right)
    = \dom\left(\left(ι^*\right)^{-1}\right)$ and
    in both cases the image of $Φ$ is $φ$.

    By Lemma \ref{thm:closability} $(ι^{-1})^*$ is densely defined
    if and only if $ι^{-1}$ is closable. But $\gr\left(ι^{-1}\right)$ is
    the same as $\gr(ι)$ except that the entries got swapped.
    Since $ι$ is continuous $\gr(ι) \subset K_0 \times K_1$
    is closed and therefore $ι^{-1}$ is closed as well.
    Hence $\left(ι^{-1}\right)^* = \left(ι^*\right)^{-1}$ is densely defined.
    That means that $ι^*$ has dense range.
  \end{proof}
  Gelfand triples are in general studied to consider
  generalised eigenvalues of operators, most prominently
  differentiation as in our case.
  An introduction to the motivation and use of                                            
  Rigged Hilbert spaces in physics can be found in
  \autocite[][]{Modino:QMRigged}. 
\end{note} % end note Gelfand triple

%
\textinput{dualities}
%
% We have already seen that $\der$ maps unitarily from
% $\HiH{\w}1$ to $\HiH{\w}0$ and in a way $\HiH{\w}0$ analogously lies
% in $\HiH{\w}{-1}$:
% Furthermore $\der$ can be used to define unitary operators between the newly introduced spaces
% as in \autocite[Theorem 2.8][9]{TUD:HSDDE}.
%
\begin{theorem}[Derivation as an unitary operator, {\autocite[Theorem 2.8][9]{TUD:HSDDE}}] \label{thm:derivative_unitary}
  Let $\w ∈ ℝ\without\{0\}$.
  Then the following mappings are unitary:
  \begin{align*}
    \der[-\w] \definedas \der[1 → 0] \colon & \HiH{-\w}1 → \HiH{-\w}0 \\
                                              & ϕ ↦ \der[-\w] ϕ \\
    \der[-\w]^{-1} \definedas \der[0 → 1] \colon & \HiH{-\w}0 → \HiH{-\w}1 \\
                                                   & ϕ ↦ \der[-\w]^{-1} ϕ \\
    \der \definedas \der[0 → -1] \colon &\HiH{\w}0 → \HiH{\w}{-1} \\
                                              & ϕ ↦ \left(\HiH{-\w}1 \ni ψ ↦ ⟨-\der[-\w]ψ, ϕ⟩_{0,0}\right) \\
    \der^{-1} \definedas \der[-1 → 0] \colon &\HiH{\w}{-1} → \HiH{\w}{0} \\
                                                   & ϕ ↦ \left(\HiH{-\w}0 \ni ψ ↦ ϕ(-\der[-\w]^{-1}ψ)\right)
  \end{align*}
  where the last definition is to be understood similarly to the identification defined in
  \ref{thm:EmbeddingHw0inHw-1}.
\end{theorem}
%
Note that %since $\w ∈ ℝ\without\{0\}$
the symbols $\der^{\pm 1}$ are used
for different operators that only differ in the range and domain. Hence while
reading one always has to take care to distinguish which domain $\der$ is
operating on.
%
\begin{proof}
  The first two mappings $\der[1 \to 0]$ and $\der[0 \to 1]$ are unitary by definition
  of $\HiH{-\w}1$.

  If one wants to define $\der (=\der[0 ⟶ -1])$ on $\HiH{\w}0$
  this new definition should coincide
  with the old one on $\dom(\der)$.
  With \eqref{eq:duality_derivative} this holds for the definition
  of $\der[0 → -1]$ for $ϕ ∈ \dom(\der) ⊂ \HiH{\w}0$.
  % With the remark \autocite[Remark 2.9][10]{TUD:HSDDE}
  % this is true: for $ϕ ∈ \dom(\der) ⊂ \HiH{\w}0$, $ψ∈ \HiH{-\w}1$ we have
  % \begin{align*}
  %   ⟨\der ϕ, ψ⟩_{0, 0}
  %   &\stackrel{\ref{thm:EmbeddingHw0inHw-1}}= ⟨\exp(-\w \mo) \exp(-\w \mo)^{-1} (\deriv + \w) \exp(-\w \mo)ϕ,
  %   \exp(\w \mo) ψ⟩_{0,0} \\
  %   &\stackrel{\text{Def. \ref{def:DifferentiationOnWeightedSpace}}}=
  %   ⟨\exp(-\w \mo) ϕ, (-\deriv + \w) \exp(\w \mo) ψ⟩_{0,0} \\
  %   &= ⟨\exp(-\w \mo) ϕ, -\exp(\w \mo) \exp(\w \mo)^{-1} (\deriv - \w) \exp(\w \mo) ψ⟩_{0,0} \\
  %   &\stackrel{\text{Def. \ref{def:DifferentiationOnWeightedSpace}}}=
  %   ⟨ϕ, \der[-\w] ψ⟩_{0,0}.
  % \end{align*}
  Fortunately the term $⟨ϕ, \der[-\w] ψ⟩_{0,0}$
  can also be defined for any $ϕ ∈ \HiH{\w}0$,
  giving motivation to the
  definition of $\der[0 ⟶ -1]$.
  To show that it is indeed isometric and therefore unitary
  verify for $ϕ ∈ \HiH{\w}0$
  with $\norm {ϕ}_{\w, 0} = 1$:
  \begin{align*}
    \norm{\der[0 ⟶ -1]ϕ}_{\w, -1}
    &= \sup_{\substack{ψ ∈ \HiH{-\w}1 \\ \norm{ψ}_{-\w, 1} = 1}}
    \abs{⟨ϕ, -\der[-\w] ψ⟩_{0,0}} \\
    &= \sup_{\substack{ψ ∈ \HiH{-\w}1 \\ \norm{ψ}_{-\w, 1} = 1}}
    \abs{⟨\exp(-\w \mo)ϕ, -\exp(\w \mo) \der[-\w] ψ⟩_{0,0}} \\
    &≤ \sup_{\substack{ψ ∈ \HiH{-\w}1 \\ \norm{ψ}_{-\w, 1} = 1}}
    \norm{\exp(-\w \mo)ϕ}_{0,0} \norm{-\exp(\w \mo) \der[-\w] ψ}_{0,0}
    \; \text{(Cauchy-Schwarz)}\\
    &= \sup_{\substack{ψ ∈ \HiH{-\w}1 \\ \norm{ψ}_{-\w, 1} = 1}}
    \norm{ϕ}_{\w,0} \norm{ψ}_{-\w,1} = 1 \\
    \text{and }
    \norm{\der[0 ⟶ -1]ϕ}_{\w, -1}
    &≥ \abs{⟨\exp(-\w \mo)ϕ, -\exp(\w \mo)\der[-\w] \underbrace{(-\der[-\w]^{-1} \exp(-2 \w \mo) ϕ}_{∈ \HiH{-\w}1})⟩_{0,0}} \\
    &= \abs{⟨\exp(-\w \mo)ϕ, \exp(-\w \mo)ϕ⟩_{0, 0}} = \norm{ϕ}_{\w, 0}^2 = 1 \\
    \text{since } 1 = \norm{ϕ}_{\w, 0}
    &= \norm{-\der[-\w]^{-1} \exp(-2 \w \mo) ϕ}_{-\w, 1}
  \end{align*}
  In order to verify that $\der[-1 ⟶ 0]$ is unitary we show that
  $\der[-1 ⟶ 0] = \der[0 ⟶ -1]^{-1}$. Let $ϕ ∈ \HiH{\w}0$. Then
  \begin{align*}
    \der[-1 ⟶ 0] \der[0 ⟶ -1] ϕ
    &= \der[-1 ⟶ 0] \left(ψ ↦ ⟨-\der[-\w]ψ, ϕ⟩\right) \\
    &= \left(ψ ↦ ⟨-\der[-\w] \left(-\der[-\w]^{-1} ψ\right), ϕ⟩_{0,0}\right) \\
    &= \left(ψ ↦ ⟨ψ, ϕ⟩_{0, 0}\right) = ϕ & \text{via the embedding of \ref{thm:EmbeddingHw0inHw-1}}
  \end{align*}
  % shown so far: \der[0 ⟶ -1] ist injektiv, unitary
  % \der[-1 ⟶ 0] ist surjektiv und auf \der[0 ⟶ 1](\Hi{\w}0 unitary
  % missing: \der[0 ⟶ -1] is surjective or \der[-1 ⟶ 0] is injective
  Since $-\der[-\w]^{-1}$ is isometric $\der[-1 ⟶ 0] ϕ = 0$ implies
  $ϕ = 0$. Hence $\der[-1 ⟶ 0]$ is injective,
  and together with $\der[-1 ⟶ 0]\der[0 ⟶ -1] = \id_{\HiH{\w}0}$
  it is bijective.
\end{proof}
%
% Now that we have an understanding how to map the regarded spaces
% onto each other with unitary maps we get the following corollary:
% \begin{corollary}[$\HiH{\w}0$ dense in $\HiH{\w}{-1}$] \label{thm:Hiw0DenseInHiw-1}
%   For $\w ∈ ℝ \without \{0\}$, $\HiH{\w}0$ is dense in $\HiH{\w}{-1}$
%   in the sense of the embedding \ref{thm:EmbeddingHw0inHw-1}.
% \end{corollary} % end corollary $\HiH{\w}0$ dense in $\HiH{\w}{-1}$
% \begin{proof}
%   We have seen in the proof of Theorem
%   \ref{thm:derivative_unitary} that $\der$ as a mapping from
%   $\HiH{\w}0$ to $\HiH{\w}{-1}$ agrees with the beforehand
%   defined $\der$ on elements of $\HiH{\w}1$.
%   Hence $\der^{-1} \colon \HiH{\w}{-1} → \HiH{\w}0$
%   maps the subset $\HiH{\w}0 \subset \HiH{\w}{-1}$ onto
%   the image of $\der^{-1} \colon \HiH{\w}0 → \HiH{\w}1$
%   which is (as a set) $\HiH{\w}1$ and therefore dense
%   in $\HiH{\w}0$. Since $\der^{-1} \colon \HiH{\w}{-1} → \HiH{\w}0$
%   is unitary by Theorem \ref{thm:derivative_unitary},
%   $\HiH{\w}0$ is also dense in $\HiH{\w}{-1}$.
% \end{proof}
\docEnd
