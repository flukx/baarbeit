%! TEX program = lualatex

\input{.maindir/tex/header/preamble-section}

\docStart
%
\section{Causality} \label{sec:causality}
%
\begin{textfold}[intro]
  In the previous sections we have seen conditions for
  ordinary differential equations to be uniquely solvable.
  Natural systems in physics have another property that should be modeled within
  this framework: causality. A solution of an equation up to any time should only
  depend on the previous behavior but not on the behavior in the future.

  We will see that the conditions on the equation must again be tightened:
  where we needed an Lipschitz estimate for some weight $\w$ we now will need that this
  estimate holds for eventually all weights, that is there is some $\w_0$ such
  that for all $\w > \w_0$ the estimate holds.

  In the end of this section we will consider causality for the solution
  operator that maps the function $F$ to the solution of the corresponding equation
  but on the way we will
  also show causality for $\der^{-1}F$, hence the definition
  regards any operator. 
\end{textfold}
\begin{definition}[Causality, {\autocite[Definition 4.1][18]{TUD:HSDDE}}] \label{def:Causality}
  Let $X, Y$ be Hilbert spaces, $\w ∈ ℝ$. A mapping
  %
  \begin{equation*}
    W \colon \Hi{\w}0 \tens X  ⊇ \dom(W) ⟶ \Hi{\w}0 \tens Y
  \end{equation*}
  is called \newTerm{causal} if for all $a ∈ ℝ$, $x, y ∈ \dom(W)$
  \begin{equation*}
    \tro(x - y) = 0 ⇒ \tro(W(x) - W(y)) = 0:
  \end{equation*}
  if two arguments do not differ for arguments $≥ a$ then the
  images do not differ as well.
  This is equivalent to (for all $a ∈ ℝ$)
  \begin{equation*}
    \tro W = \tro W \tro.
  \end{equation*}
  Here $\tro$ is to be understood as in Definition \ref{note:defexp}:
  \begin{equation*}
    (\tro x) (t) = \chFct_{ℝ_{< a}} (t) x (t) =
    \begin{cases}
      x(t) & t < a \\
      0 & t \geq a
    \end{cases}.
  \end{equation*}
\end{definition} % end definition of Causality
\begin{proof}[Equivalence] \enquote{$⇒$}:
  For any $x ∈ \dom(W)$ we obviously have $\tro(x - \tro x) = 0$ and hence
  with the linearity of $\tro$
  \begin{align*}
    \tro(W(x) - W(\tro x)) = 0 ⇒ \tro W(x) = W(\tro x).
  \end{align*}
  %
  \enquote{$⇐$}:
  Let $x, y ∈ \dom(W)$.
  $\tro(x - y) = 0$ implies $\tro x = \tro y$ by linearity of $\tro$. Hence
  \begin{align*}
    \tro(W(x) - W(y)) &= \tro W(\tro x ) - \tro W(\tro y) \\
                      &= \tro W(\tro x) - \tro W(\tro x) = 0. && \qedhere
  \end{align*}
\end{proof}
\begin{textfold}[intro general integral]
  The first step to prove the causality of the solution operator is
  to prove the causality of the contraction $\der^{-1}F_{\w}$.
  For this proof we need to relate contraction on weighted spaces
  for different weights and hence need a $\w$-independent formulation
  of $\der^{-1}$. This is already commented on at the Definition \ref{def:C+}
  of the test space $\tstp+$.
\end{textfold}
\begin{definition}[$∫$ on $\tstp+$, {\autocite[Definition~4.3][18]{TUD:HSDDE}}] \label{def:Intontstp}
  Let $w ∈ \dual{\tstp+}$. Then define
  \begin{equation*}
    ∫_{-∞}^{·} w \colon \tstp+ ⟶ ℂ \colon ψ ⟶ w\left(∫_{·}^{∞} ψ\right).
  \end{equation*}
  As usual we identify $w$ with an element of $\HiH{\w}{-1}$
  if it continuous with respect to the $\HiH{\w}{-1}$-norm.
  Then, as we expected, we have for $ψ ∈ \tstp+$ and $\w > 0$:
  \begin{equation*}
    \left(∫_{-∞}^{·} w\right)(ψ) = w \left(∫_{·}^{∞} ψ\right)
    \stackrel{\eqref{eq:integralnegw}}= w(-\der[-\w]^{-1}ψ)
    \stackrel{\ref{thm:derivative_unitary}}= \der^{-1}w(ψ).
  \end{equation*}
\end{definition} % end definition of $∫$ on $\tstp+$

\begin{theorem}[Causality of Lipschitz function, {\autocite[Theorem~6.3][19]{TUD:HSDDE}}] \label{thm:Lipschitzcausal}
  Let $\w_0 ∈ ℝ_{> 0}$,
  $F \colon \tstH ⟶ \dual{\tstp+}$ be
  such that there exists $L ∈ ℝ_{\geq 0}$ and for each
  $\w > \w_0$
  there exist
  $K ∈ ℝ_{> 0}$
  such that for all
  $u, w ∈ \tstH$ and $ψ ∈ \tstp+$
  \begin{equation*}
    \abs{F(0)(ψ)} ≤ K\norm{ψ}_{- \w, 1} \text{ and }
    \abs{F(u)(ψ) - F(w)(ψ)} ≤ L \norm{ψ}_{-\w, 1} \norm{u - w}_{\w, 0}.
  \end{equation*}
  Then for $\w ∈ ℝ_{> \w_0}$ the mapping
  \begin{equation*}
    \der^{-1} F_{\w} \colon \HiH{\w}0 ⟶ \HiH{\w}0
  \end{equation*}
  is causal. Here $F_{\w}$ is the continuous extension of $F$
  to a mapping $\HiH{\w}0 ⟶ \HiH{\w}{-1}$ as before.
\end{theorem}
Note that the assumption on $F$ becomes different to the existence theorems by assuming
the Lipschitz estimate for eventually all weights but with an arbitrary Lipschitz constant.
\begin{proof}
  First we start with smooth functions for which we have formulas to
  calculate with.
  Let $\w ∈ ℝ_{> \w_0}$, $v ∈ \tstH$, $a ∈ ℝ$. We have to show
  \begin{equation*}
    \tro \der^{-1} F_{\w}v = \tro \der^{-1} F_{\w} \tro v.
  \end{equation*}
  $\der^{-1} F_{\w}$ maps to $\HiH{\w}0$ but when written out
  we only have the formulation as a mapping to $\Hidual{\HiH{-\w}0}$
  from Theorem \ref{thm:derivative_unitary}.
  What does it mean to apply $\tro$ to an element of $\Hidual{\HiH{-\w}0}$?
  For $w ∈ \HiH{\w}0 \isom \Hidual{\HiH{-\w}0}$, $ψ ∈ \HiH{-\w}0$ we have
  %
  \begin{equation*}
    \begin{split}
      (\tro w)ψ
      &= ⟨\tro w, ψ⟩_{0,0}
      = ∫_{ℝ} ⟨\tro w(x), ψ(x)⟩_H \D x \\
      &= ∫_{ℝ_{< a}} ⟨w(x), ψ(x)⟩_H \D x
      = ∫_{ℝ} ⟨w(x), \tro ψ(x)⟩_H \D x \\
      &= ⟨w, \tro ψ⟩_{0, 0} = w(\tro ψ)
    \end{split}
  \end{equation*}
  %
  Hence for checking $\tro w = \tro \tilde{w}$ we only have to test the equality
  on 
  \begin{equation*}
    ψ ∈ \tstH ⊂ \HiH{-\w}0 \text{ with } \sup \supp ψ \leq a.
  \end{equation*}
  %
  \begin{equation}\label{eq:testLimitedSupport}
    \text{We have to show: } \der^{-1} F_{\w}(v)(ψ) = \der^{-1} F_{\w}(\tro v)(ψ).
  \end{equation}
  %
  $\tro v$ is not smooth in $a$, hence cannot plugged into $F$.
  In order to approximate $\tro v$ with smooth functions, we replace
  $\tro$ with $ϕ(\mo)$ with $ϕ ∈ \tstsmD{}{∞}(ℝ; H)$ and $\sup \supp ϕ ≤ a$.
  Then for any $η ∈ ℝ_{≥ \w_0}$
  \begin{align*}
    \abs{\der^{-1} F_{\w}(v)(ψ) - \der^{-1} F_{\w}(ϕ(\mo)v)(ψ)}
    \overset{\text{Def.~\ref{def:Intontstp}}}&=
    \abs{\left(∫_{-∞}^{·} F(v)\right)(ψ) - \left(∫_{-∞}^{·} F(ϕ(\mo)v)\right)(ψ)} \\
    &= \abs{\der[η]^{-1}F_{η}(v)(ψ) - \der[η]^{-1}F_{η}(ϕ(\mo)v)(ψ)} \\
    &\qquad \text{since the continuity estimate holds for any} \\
    &\qquad \text{$η ≥ \w_0$ and hence $F_{η}$ exists} \\
    &= \abs{F_{η}(v)(-\der[-η]^{-1}ψ) - F_{η}(ϕ(\mo)v)(-\der[-η]^{-1}ψ)} \\
    &\qquad \text{(Def.\ \ref{thm:derivative_unitary}:
  $\der[η]^{-1}$ on $\HiH{η}{-1}$)} \\
  &≤ L \norm{-\der[-η]^{-1}ψ}_{-η, 1} \norm{v - ϕ(\mo)v}_{η, 0} \\
  &\qquad \text{since $F_{η}$ is Lipschitz continuous} \\
  &= L \norm{ψ}_{-η, 0} \norm{v - ϕ(\mo)v}_{η, 0} \\
  &≤ L \norm{ψ}_{0, 0} \exp(η a) \norm{v - ϕ(\mo)v}_{η, 0}
\end{align*}
since
\begin{align*}
  \norm{ψ}_{-η, 0}^2
  &= ∫_{ℝ}\norm{ψ(t)}_H^2\exp(2ηt) \D t
  = ∫_{∞}^{a} \norm{ψ(t)}_H^2 \exp(2ηt) \D t \\
  &≤ ∫_{ℝ} \norm{ψ(t)}_H^2 \D t \exp(2 η a)
  = \norm{ψ}_{0,0}^2 \exp(ηa)^2 < ∞ \text{ since }ψ ∈ \tstH. %⊂ \Hi00 \tens H.
\end{align*}
By continuity we can again replace $ϕ$ by $\chFct_{ℝ_{<a}}$. To see that
consider any $ϕ$ that agrees with $\chFct_{ℝ_{<a}}$ except for some small
interval $(a - ε, a)$. There $v$ is bounded. Consider the difference
%
\begin{align*}
  \norm{(ϕ - \chFct_{ℝ < a})(\mo)v}_{η, 0} ≤
  %\underbrace{
  \norm{ϕ - \chFct_{ℝ_{<a}}}_{η, 0} %}_{⟶ 0}
  \underbrace{\sup_{x ∈ (a - ε)} \norm{v(x)}_H}_{< ∞}.
\end{align*}
By choosing $ϕ$ sufficiently close to $\chFct_{ℝ_{<a}}$,
we can approximate $\chFct_{ℝ_{<a}}(m)v$ arbitrarily close
with $ϕ(m)v$.

We get
\begin{align*}
  \abs{\der^{-1} F_{\w}(v)(ψ) - \der^{-1} F_{\w}(ϕ(\mo)v)(ψ)}
  &≤ L \norm{ψ}_{0, 0} \exp(η a) \norm{v - \tro v}_{η, 0} \\
  &= L \norm{ψ}_{0, 0} \exp(η a) \norm{\tro[≥ a] v}_{η, 0} \\
  &= L \norm{ψ}_{0, 0} \left(∫_a^{∞} \norm{v(t)}_H^2 \exp(-2ηt) \exp(2 η a) \D t\right)^{\frac 12} \\
  &= L \norm{ψ}_{0, 0}
  \left(∫_0^{∞} \norm{v(s + a)}_H^2 \underbrace{\exp(-2ηs)}_{→ 0,\, η → ∞}
  \D s\right)^{\frac 12} \\ & \qquad (s = t - a) \\
  ⟶ 0 \text{ for } &η ⟶ ∞ \text{ by the dominated convergence theorem}
\end{align*}
As we discussed for \eqref{eq:testLimitedSupport} in the beginning, this means
%
\begin{equation*}
  \tro \left(\der^{-1}F_{\w}(v) - \der^{-1}F_{\w}(\tro v)\right) = 0.
\end{equation*}
%
Since $\tro$ and $\der^{-1}F_{\w}$ are continuous, this also holds
for arbitrary $v ∈ \HiH{\w}0$.
\end{proof}
%
\docEnd
