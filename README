To compile any part of the thesis go to the directory tex and run
latexmk -lualatex to-be-compiled-file.tex
In order to compile the entire thesis, run
latexmk -lualatex main.tex

Die Hauptdatei (die ganze Arbeit) ist tex/main.tex. Es beinhaltet mit einem modifizierten \input-Befehl die verschiedenen Kapitel. Alle Dateien mit LaTeX-Code liegen in /tex. Die tikz-Zeichnungen liegen im Ordner /zeichnungen. (Auf branch master z.Z. keine vorhanden.)

Man kann jede Datei im Ordner /tex einzeln kompilieren. Dafür sind einige Hürden zu nehmen gewesen:
• Pfade von Dateien, die mit \input eingebunden werden, müssen funktionieren, egal, wo die Datei liegt. Das ist dadurch gelöst, dass alle \input-Anweisungen durch \maininput, \textinput, \tikzinput ersetzt wurden. Diese setzen ".maindir/" vor den Pfad. Das ist ein symlink, der in allen Ordnern liegt und jeweils zum Hauptordner referenziert. Diese Links können automatisch erzeugt werden mit dem python-skript .maindir/scripts/maindircreate.py (muss im Hauptordner ausgeführt werden).
• Die Präambel darf nur einmal eingebunden werden. Um dies zu erreichen, fügt keine Datei die Präambel direkt ein (mit input), sondern alle Dateien binden mit \input{.maindir/tex/header/preamble-section} einen Wrapper ein, der die Präambel nur lädt, wenn der Wrapper bisher noch nicht aufgerufen wurde. Dort werden auch die Ersatzbefehle für \begin und \end{document} definiert: docStart und docEnd. Damit docEnd im richtigen Fall das Dokument beendet läuft der counter filedepthScript mit, der anzeigt, in welcher Einfügetiefe wir uns befinden.
• Der Counter filedepthScript wird erstellt in der Präambel (die nur einmal aufgerufen wird), erhöht und verringert beim Aufruf von \maininput (und damit auch bei \textinput und \tikzinput). Im gerade kompilierten "Hauptdokument" ist der Counter auf 0, in allen weiteren auf >0.

• Die Datei tex/_TEMPLATE.tex beinhaltet die Dinge, die in jede (Unter-)datei, die auch alleine kompilieren soll, rein muss.
